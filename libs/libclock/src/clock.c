#include <clock/clock.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <cspace/cspace.h>
#define IRQ_TABLE_SIZE 256

#define verbose 0

#define conditional_panic(a, b) __conditional_panic(a, b, __FILE__, __func__, __LINE__)
#define panic(b) conditional_panic(1, b)
#define MILLISCOND2TICK(x) (20 * (x) - 1)
#define TICK2MILLISCOND(x) (((x) + 1)/20)
#define MAX_DELAY 0xFFFFFFFF
#define ONE_DAY 0x66FF3000

struct clk_irq {
    uint32_t next_id;
    timer_callback_t callback;
    void* data;
    timestamp_t due;
}; 

struct clock {
    timestamp_t stamp_base;
    seL4_IRQHandler cap;
    seL4_IRQHandler timecap;
};

static struct clk_irq irq_table[IRQ_TABLE_SIZE];
static volatile struct epit_register* pEpitReg = NULL;
static volatile struct epit_register* pTimeReg = NULL;
static struct clock* pClock=NULL;

uint8_t insert_timer_node(uint32_t id);
uint32_t pop_timer_node();

int start_timer(seL4_CPtr interrupt_ep)
{
    int err;

    if(!pClock)
        pClock = (struct clock*)malloc(sizeof(struct clock));

    if(!pClock)
    {
        dprintf(1,"Failed to alloc Clock struct\n");
        return 0;
    }

    memset(pClock,0,sizeof(struct clock));

    //MAX_DELAY means no timer regist in clock table
    if(!pTimeReg)
        pTimeReg = (struct epit_register*)map_device((void*)0x020D4000,sizeof(struct epit_register));

    //change clock source
    //1.set EN = 0
    pTimeReg->CR &= ~EPIT1_CR_EN;
    //2.set OM =00
    pTimeReg->CR &= ~EPIT1_CR_OM;

    //3.disable EPIT interupt
    pTimeReg->CR &= ~EPIT1_CR_OCIEN;

    //set prescaler 3300 times go to counter
   pTimeReg->CR |= (3299<< EPIT1_CR_PCAL_OFF);
   pTimeReg->LR = MAX_DELAY;
   pTimeReg->CMPR = 0;
   pTimeReg->CR |= EPIT1_CR_IOVW; //set IOVW
   pTimeReg->CR |= EPIT1_CR_RLD; //set RLD = 1
   //4.set clock source to peripheral clock
    pTimeReg->CR |=(0x1 << EPIT1_CR_SRC_OFF);

    //5.clear EPIT status register
    pTimeReg->SR = ONE_DAY;
    //6.set ENMOD
    pTimeReg->CR |= EPIT1_CR_ENMOD;
    //7. set EN=1
    pTimeReg->CR |= EPIT1_CR_EN;

    //8.enalbe interupt
    pTimeReg->CR |= EPIT1_CR_OCIEN;

    //GPT->87,EPIT1->88,EPIT2->89
    pClock->timecap = cspace_irq_control_get_cap(cur_cspace, seL4_CapIRQControl, 89);
    conditional_panic(!(pClock->timecap), "Failed to acquire and IRQ control cap in clock");
    /* Assign to an end point */
    err = seL4_IRQHandler_SetEndpoint(pClock->timecap, interrupt_ep);
    conditional_panic(err, "Failed to set interrupt endpoint in clock");
    /* Ack the handler before continuing */
    err = seL4_IRQHandler_Ack(pClock->timecap);
    conditional_panic(err, "Failure to acknowledge pending interrupts in clock");

    /////////////////////////////////////////////////////////////////////////////////////////////////
    if(!pEpitReg)
        pEpitReg = (struct epit_register*)map_device((void*)0x020D0000,sizeof(struct epit_register));

    if(!pEpitReg)
    {
        dprintf(1,"Failed to map EPIT device register\n");
        return 0;
    }

    //change clock source
    //1.set EN = 0
    pEpitReg->CR &= ~EPIT1_CR_EN;
    //2.set OM =00
    pEpitReg->CR &= ~EPIT1_CR_OM;

    //3.disable EPIT interupt
    pEpitReg->CR &= ~EPIT1_CR_OCIEN;

    //set prescaler 3300 times go to counter
    pEpitReg->CR |= (3299<< EPIT1_CR_PCAL_OFF);
    pEpitReg->LR = MAX_DELAY;
    pEpitReg->CMPR = 0;
    pEpitReg->CR |= EPIT1_CR_IOVW; //set IOVW
    pEpitReg->CR |= EPIT1_CR_RLD; //set RLD = 1
    //4.set clock source to peripheral clock
    pEpitReg->CR |=(0x1 << EPIT1_CR_SRC_OFF);

    //5.clear EPIT status register
    pEpitReg->SR = 0xFFFFFFFF;
    //6.set ENMOD
    pEpitReg->CR |= EPIT1_CR_ENMOD;
    //7. set EN=1
    pEpitReg->CR |= EPIT1_CR_EN;

    //8.enalbe interupt
    pEpitReg->CR |= EPIT1_CR_OCIEN;

    //GPT->87,EPIT1->88,EPIT2->89
    pClock->cap = cspace_irq_control_get_cap(cur_cspace, seL4_CapIRQControl, 88);
    conditional_panic(!(pClock->cap), "Failed to acquire and IRQ control cap in clock");
    /* Assign to an end point */
    err = seL4_IRQHandler_SetEndpoint(pClock->cap, interrupt_ep);
    conditional_panic(err, "Failed to set interrupt endpoint in clock");
    /* Ack the handler before continuing */
    err = seL4_IRQHandler_Ack(pClock->cap);
    conditional_panic(err, "Failure to acknowledge pending interrupts in clock");
    
    return pClock->cap;
}

uint32_t register_timer(uint64_t delay, timer_callback_t callback, void *data)
{
    int i;
    //delay time is too long to regist it
    if(delay >= MAX_DELAY)
        return 0;

    for(i = 1;i < IRQ_TABLE_SIZE ;i ++)
    {
        if(irq_table[i].callback == NULL)
        {
            irq_table[i].callback = callback;
            irq_table[i].data = data;
            irq_table[i].due = time_stamp() + delay - 1;

            break;
        }
    }
    if(i == IRQ_TABLE_SIZE)
    {
        dprintf(1,"time table is packed now\n");
        return 0;
    }

    //if pNode insert in the head of the list,change the clock load register value
    if(insert_timer_node(i))
    {
        pEpitReg->LR = MILLISCOND2TICK(irq_table[irq_table[0].next_id].due - time_stamp());
    }
    return i;
}

int remove_timer(uint32_t id)
{
    if(id < 1 || id > IRQ_TABLE_SIZE - 1 || irq_table[id].callback == NULL)
        return 0;

    uint32_t headid =0;
    while(irq_table[headid].next_id !=id)
        headid = irq_table[headid].next_id;

    irq_table[headid].next_id = irq_table[id].next_id;

    irq_table[id].callback = NULL;
    irq_table[id].data = NULL;
    irq_table[id].due = 0;
    irq_table[id].next_id = 0;
    return 0;
}

int timer_interrupt(void)
{
    //two hardware clock may request interrupt same time
    //both of those statue regist need check
    if(pTimeReg->SR)
    {
        pClock->stamp_base++;
        pTimeReg->SR = 0xFFFFFFFF;
        seL4_IRQHandler_Ack(pClock->timecap);
    }

    while(pEpitReg->SR)
    {
        uint32_t id = 0;
        while(id = pop_timer_node())
        {
            irq_table[id].callback(id,irq_table[id].data); 
            irq_table[id].callback = NULL;
            irq_table[id].data = NULL;
            irq_table[id].due = 0;
            irq_table[id].next_id = 0;

            //race conditoin avoid
            if(pTimeReg->SR)
            {
                pClock->stamp_base++;
                pTimeReg->SR = 0xFFFFFFFF;
                seL4_IRQHandler_Ack(pClock->timecap);
            }
        }

        if(irq_table[0].next_id == 0)
        {
            pEpitReg->LR = MAX_DELAY;
        }
        else
        {
            pEpitReg->LR = MILLISCOND2TICK(irq_table[irq_table[0].next_id].due - time_stamp());
        }

        //reset statue register
        pEpitReg->SR = 0xFFFFFFFF;
        seL4_IRQHandler_Ack(pClock->cap);

    }
    return 0;
}

timestamp_t time_stamp(void)
{
    volatile uint32_t tick = pTimeReg->LR - pTimeReg->CNR;
    return TICK2MILLISCOND((pClock->stamp_base* ONE_DAY) + tick);
}

int stop_timer(void)
{
    seL4_IRQHandler_Clear(pClock->cap);
    seL4_IRQHandler_Clear(pClock->timecap);
    pEpitReg->CR &= ~EPIT1_CR_EN;
    //2.set OM =00
    pEpitReg->CR &= ~EPIT1_CR_OM;
    //3.disable EPIT interupt
    pEpitReg->CR &= ~EPIT1_CR_OCIEN;

    pTimeReg->CR &= ~EPIT1_CR_EN;
    //2.set OM =00
    pTimeReg->CR &= ~EPIT1_CR_OM;
    //3.disable EPIT interupt
    pTimeReg->CR &= ~EPIT1_CR_OCIEN;

    free(pClock);
    pClock = NULL;
    return 0;
}

uint8_t insert_timer_node(uint32_t id)
{
    uint8_t need_change_clock = 1;
    uint32_t head_id = 0;
    while(irq_table[head_id].next_id != 0 && irq_table[irq_table[head_id].next_id].due <= irq_table[id].due)
    {
        head_id = irq_table[head_id].next_id;
        need_change_clock = 0;
    }

    irq_table[id].next_id = irq_table[head_id].next_id;
    irq_table[head_id].next_id = id;
    return need_change_clock;
}

uint32_t pop_timer_node()
{
    uint32_t id = 0;
    if(irq_table[irq_table[0].next_id].due <= time_stamp())
    {
        id = irq_table[0].next_id;
        irq_table[0].next_id = irq_table[id].next_id;
    }

    return id;
}


