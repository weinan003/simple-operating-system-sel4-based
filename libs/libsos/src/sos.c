/*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <sos.h>

#include <sel4/sel4.h>
#define MIN(a,b) (((a)<(b))?(a):(b))

int sos_sys_open(const char *path, fmode_t mode) 
{
    seL4_MessageInfo_t tag;
    size_t length = 0;
    if(path)
        length = strlen(path);
    tag = seL4_MessageInfo_new(0, 0, 0, 4);
    seL4_SetMR(0,SOS_SYSCALL_OPEN);
    seL4_SetMR(1,mode);
    seL4_SetMR(2,length);
    seL4_SetMR(3,(seL4_Word)path);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    return seL4_GetMR(0);
}

int sos_sys_read(int file, char *buf, size_t nbyte) 
{
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 4);
    seL4_SetMR(0,SOS_SYSCALL_READ);
    seL4_SetMR(1,file);
    seL4_SetMR(2,(seL4_Word)buf);
    seL4_SetMR(3,nbyte);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    return seL4_GetMR(0);
}

int sos_sys_write(int file, const char *buf, size_t nbyte) 
{
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 4);
    seL4_SetMR(0,SOS_SYSCALL_WRITE);
    seL4_SetMR(1,file);
    seL4_SetMR(2,(seL4_Word)buf);
    seL4_SetMR(3,nbyte);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    return seL4_GetMR(0);
}

int sos_sys_close(int file) 
{
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    seL4_SetMR(0,SOS_SYSCALL_CLOSE);
    seL4_SetMR(1,file);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    return seL4_GetMR(0);
}

seL4_Word sos_sys_brk(seL4_Word vaddr) {
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    seL4_SetMR(0,SOS_SYSCALL_BRK);
    seL4_SetMR(1,vaddr);
    seL4_Call(SOS_IPC_EP_CAP, tag);

    //if err return 0 else return value
    return seL4_GetMR(0);
}


void sos_sys_usleep(int msec) {
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    seL4_SetMR(0,SOS_SYSCALL_SLEEP);
    seL4_SetMR(1,msec);
    seL4_Call(SOS_IPC_EP_CAP,tag);
}

int64_t sos_sys_time_stamp(void) {
    seL4_MessageInfo_t tag;
    int64_t ret;

    tag = seL4_MessageInfo_new(0, 0, 0, 1);
    seL4_SetMR(0,SOS_SYSCALL_TIME_STAMP);
    seL4_Call(SOS_IPC_EP_CAP, tag);
    memcpy(&ret, seL4_GetIPCBuffer()->msg, sizeof(ret));
    ret *= 1000;
    return ret;
}

pid_t sos_process_create(const char *path) {
    seL4_MessageInfo_t tag;

    size_t length;
    length = strlen(path);
    tag = seL4_MessageInfo_new(0, 0, 0, 3);
    seL4_SetMR(0,SOS_SYSCALL_PROC_CREATE);
    seL4_SetMR(1,length);
    seL4_SetMR(2,(seL4_Word)path);
    seL4_Call(SOS_IPC_EP_CAP,tag);

    return (pid_t)seL4_GetMR(0);

}

int sos_process_delete(pid_t pid) 
{
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    seL4_SetMR(0,SOS_SYSCALL_PROC_DELETE);
    seL4_SetMR(1,pid);
    seL4_Call(SOS_IPC_EP_CAP,tag);

    return seL4_GetMR(0);
}

pid_t sos_my_id(void) {
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 1);
    seL4_SetMR(0,SOS_SYSCALL_PROC_PID);
    seL4_Call(SOS_IPC_EP_CAP,tag);

    return (pid_t) seL4_GetMR(0);
}

int sos_process_status(sos_process_t *processes, unsigned max) {
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    int i;
    for(i = 0;i < max;i ++)
    {
        seL4_SetMR(0,SOS_SYSCALL_PROC_STATUS);
        seL4_SetMR(1,i);
        seL4_Call(SOS_IPC_EP_CAP,tag);

        if (seL4_GetMR(0)) break;

        processes[i].pid = seL4_GetMR(1);
        processes[i].size = seL4_GetMR(2);
        memcpy(&(processes[i].stime),(seL4_GetIPCBuffer()->msg + 3),sizeof(int64_t));
        memcpy((processes[i].command),(seL4_GetIPCBuffer()->msg + 5),N_NAME);
    }

    return i;
}

pid_t sos_process_wait(pid_t pid) {
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    seL4_SetMR(0,SOS_SYSCALL_PROC_WAIT);
    seL4_SetMR(1,pid);
    seL4_Call(SOS_IPC_EP_CAP,tag);

    return (pid_t) seL4_GetMR(0);
}

int sos_stat(const char *path, sos_stat_t *buf)
{
    seL4_MessageInfo_t tag;
    size_t length;
    length = strlen(path);
    tag = seL4_MessageInfo_new(0, 0, 0, 3);
    seL4_SetMR(0,SOS_SYSCALL_STAT);
    seL4_SetMR(1,length);
    seL4_SetMR(2,(seL4_Word)path);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    if(seL4_GetMR(0) == -1)
    {
        return -1;
    }
    else
    {
        buf->st_type = seL4_GetMR(1);
        buf->st_fmode= seL4_GetMR(2);
        buf->st_size = seL4_GetMR(3);
        buf->st_ctime= seL4_GetMR(4);
        buf->st_atime= seL4_GetMR(5);
    }
    return 0;
}

int sos_getdirent(int pos, char *name, size_t nbyte)
{
    seL4_MessageInfo_t tag;
    tag = seL4_MessageInfo_new(0, 0, 0, 4);
    seL4_SetMR(0,SOS_SYSCALL_GETDIRENT);
    seL4_SetMR(1,pos);
    seL4_Call(SOS_IPC_EP_CAP,tag);
    if(seL4_GetMR(0))
    {

        return -1;
    }
    else
    {
        size_t size = MIN(seL4_GetMR(1),nbyte);
        strncpy(name,(char*)(seL4_GetIPCBuffer()->msg + 2), size );
        if (size > 0)
            name[size] = '\0';

        return size;
    }
}

size_t sos_write(void *vData, size_t count) {
    sos_sys_write(1, vData, count);
    return count;
}
