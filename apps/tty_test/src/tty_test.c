/*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

/****************************************************************************
 *
 *      $Id:  $
 *
 *      Description: Simple milestone 0 test.
 *
 *      Author:			Godfrey van der Linden
 *      Original Author:	Ben Leslie
 *
 ****************************************************************************/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <sel4/sel4.h>
#include <sos.h>

#include "ttyout.h"
#define NPAGES 27
// Block a thread forever
// we do this by making an unimplemented system call.

#define NPAGES 27
#define TEST_ADDRESS 0x20000000
#define SMALL_BUF_SZ 2
#define BUF_SIZ 100

char test_str[] = "Basic test string for read/write\n";
char small_buf[SMALL_BUF_SZ];
int test_buffers(int console_fd) {
 //      /* test a small string from the code segment */
 //      int result = sos_sys_write(console_fd, test_str, strlen(test_str));

 //      /* test reading to a small buffer */
 //      result = sos_sys_read(console_fd, small_buf, SMALL_BUF_SZ);
 //      sos_sys_write(console_fd, small_buf, result);
 //      /* make sure you type in at least SMALL_BUF_SZ */

 //      printf("test stack buffer:\n");
 //      /* test a reading into a large on-stack buffer */
 //      char stack_buf[BUF_SIZ];
 //      /* for this test you'll need to paste a lot of data into 
 //         the console, without newlines */
 //      result = sos_sys_read(console_fd, &stack_buf, BUF_SIZ);

 //      result = sos_sys_write(console_fd, &stack_buf, BUF_SIZ);

 //      printf("test heap buffer:\n");
 //      /* this call to malloc should trigger an sbrk */
 //      char *heap_buf = malloc(BUF_SIZ);

 //      /* for this test you'll need to paste a lot of data into 
 //         the console, without newlines */
 //      result = sos_sys_read(console_fd, heap_buf, BUF_SIZ);

 //      result = sos_sys_write(console_fd, heap_buf, result);

 //      printf("test sleep:\n");
       /* try sleeping */
       for (int i = 0; i < 5; i++) {
           uint64_t prev_seconds = time(NULL);
           sleep(1);
           uint64_t next_seconds = time(NULL);
           assert(next_seconds > prev_seconds);
           printf("Tick\n");
       }
   }

int main(void){
    pid_t pid = sos_my_id();
//    printf("test process: %d \n",pid);

//    for (int i = 0; i < 1; i++) {
//        uint64_t prev_seconds = time(NULL);
//        sleep(1);
//        uint64_t next_seconds = time(NULL);
//        assert(next_seconds > prev_seconds);
//        printf("Tick\n");
//    }

    /* test 1: recursive create process */
    //sos_process_create("tty_test");


    /* test 2: create and wait */
//    if(pid < 2)
//    {
//        pid_t chd_id = sos_process_create("tty_test");
//        sos_process_wait(chd_id); 
//    }

    /* test 3: test process wait cooperate with sosh */
   // sos_process_wait(-1);
    printf("process %d before exits\n",pid);

//    int fd = sos_sys_open("console",2);
//    test_buffers(fd);
    return 0;
}
