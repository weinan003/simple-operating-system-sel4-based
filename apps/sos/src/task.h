#ifndef __SOS__TASK__
#define __SOS__TASK__
#include <sel4/sel4.h>
#include <sync/mutex.h>
#include <sync/sem.h>

#define SOS_TASK_OK     0
#define SOS_TASK_FAILE  1

#define SOS_TASK_BADGE  (1000)
typedef void (*task_fun)(void);

typedef struct msg_ep
{
    seL4_EndpointCap cap;
    seL4_Word addr;
}msg_ep;

typedef struct task_node
{
    seL4_Word tcb_addr;
    seL4_TCB tcb_cap;

    seL4_Word ipc_addr;
    seL4_CPtr ipc_cap;
    seL4_CPtr ipc_vaddr;

    seL4_Word stack_addr[4];
    seL4_CPtr stack_cap[4];
}task_node;

/*MESSAGE QUEUE data structure*/
struct msg
{
    size_t length;
    uint32_t para_0;
    uint32_t para_1;
    uint32_t para_2;
    uint32_t para_3;
    uint32_t para_4;

    seL4_Sosvaddr vaddr;
    seL4_CPtr reply_cap;
    struct msg* next;
};

typedef struct msg_queue
{
    struct msg* head_;
    struct msg* tail_;
    sync_sem_t _sem;
    sync_mutex_t _mux;
}msg_queue;

typedef struct 
{
    int tid;
    int isready;
    /* 这个工作thread的通信cap,可以通过这个reply_cap唤醒这个thread */
    seL4_CPtr reply_cap;
    struct msg *head_,*tail_;
}subthd_status;

task_node* sos_task_create(task_fun tfunction,seL4_CPtr fault_ep,void *para,int badge,int id);
void sos_task_destory(task_node* node);

msg_queue* _msg_queue_init(void);
struct msg* _msg_queue_pop_front(msg_queue *);
void _msg_queue_push_back(msg_queue *,struct msg *);


void* sync_new_ep(seL4_CPtr* ep, int badge);
void sync_free_ep(void* ep);

#endif
