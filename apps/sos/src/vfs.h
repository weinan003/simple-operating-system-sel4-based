#ifndef __VFS__
#define __VFS__
#include <sos.h>
#include "pcb.h"
#include "file_callback.h"
#include "env.h"

#define FILE_TABLE_SIZE (128 * 32)
#define MAX_DEVICE_NUMBER (2)


//TODO::due to memory limination,only support small vfs table
typedef struct sos_vfs_operation sos_vfs_operation;
typedef struct sos_vfs_vnode sos_vfs_vnode;

struct sos_vfs_operation
{
    int (*vop_open)(seL4_File_Descripter local_id,seL4_File_Descripter *glb_fd,sos_vfs_vnode **p_vnode,seL4_Buffer path,seL4_CPtr reply_cap);
    int (*vop_close)(sos_vfs_vnode *p_vnode);
    int (*vop_read)(sos_as *pcb,nfs_inode *inode,uint32_t *offset,seL4_Uservaddr ubuf,size_t length,seL4_CPtr reply_cap);
    int (*vop_write)(sos_as *pcb,nfs_inode *inode,uint32_t *offset,seL4_Uservaddr ubuf,size_t length,seL4_CPtr reply_cap);
    int (*vop_stat)(seL4_Buffer path, seL4_CPtr reply_cap);
    int (*vop_getdirent)(int pos,seL4_CPtr reply_cap);
};

struct sos_vfs_vnode
{
    sos_vfs_operation* op;
    fmode_t permission;
    uint32_t pid;
    uint32_t offset;
    nfs_inode *inode;
};

int sos_vfs_init(void);
int sos_vfs_set_op(sos_vfs_operation *op,uint32_t id);
sos_vfs_operation* sos_vfs_create_op(void);

void sos_vfs_open(sos_as *pcb,seL4_File_Descripter local_id,seL4_Buffer path,fmode_t mode,seL4_CPtr reply_cap);
void sos_vfs_read(sos_as *pcb,seL4_File_Descripter glb_index,seL4_Uservaddr cbuf,size_t lenght,seL4_CPtr reply_cap);
void sos_vfs_write(sos_as *pcb,seL4_File_Descripter glb_index,seL4_Uservaddr ubuf,size_t lenght,seL4_CPtr reply_cap);
void sos_vfs_stat(seL4_Buffer path,seL4_CPtr reply_cap);
void sos_vfs_getdirent(int pos,seL4_CPtr reply_cap);
void sos_vfs_seek_operation(int id,sos_vfs_operation **op);
void sos_vfs_close(sos_as *pcb,seL4_File_Descripter local_id,seL4_CPtr reply_cap);

seL4_File_Descripter sos_vfs_create_vnode(sos_as* pcb);
sos_vfs_vnode* sos_vfs_seek_vnode(seL4_File_Descripter glb_id);
void sos_vfs_delete_vnode(sos_as* pcb,seL4_File_Descripter glb_id);

#endif
