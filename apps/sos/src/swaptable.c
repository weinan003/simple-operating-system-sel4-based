#include <ut_manager/ut.h>
#include <cspace/cspace.h>
#include "swaptable.h"
#include "frametable.h"
#include "vmem_layout.h"
#include <assert.h>
#include "lock.h"
#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include <clock/clock.h>
#include "mapping.h"
#include "file.h"
#include "file_callback.h"
#define SWAP_PAGEBITS (31)

/* swap file的handler */
extern fhandle_t mnt_point ;
fhandle_t NFS_SWAP_FILE;
extern uint32_t v_mem_off;
extern uint32_t _time_base;

	void 
swap_init()
{
    int err;
    uint32_t size,page;
    uint32_t addr,head_offset;
    seL4_CPtr cap;

    head_offset = v_mem_off;
    size = sizeof(union u_swap) * (1 << ( SWAP_PAGEBITS - seL4_PageBits));
    page = size >> seL4_PageBits;

    for(int i = 0;i < page ;i++)
    {
        addr = ut_alloc(seL4_PageBits);
        err = cspace_ut_retype_addr(addr, 
                seL4_ARM_SmallPageObject,
                seL4_PageBits,
                cur_cspace,
                &cap);
        conditional_panic(err,"Failed to retype frame table addr");

        err = map_page(cap,seL4_CapInitThreadPD,
                PROCESS_HEAP_START + (v_mem_off<<seL4_PageBits),
                seL4_AllRights,seL4_ARM_PageCacheable);


        conditional_panic(err,"map_page Failed");
        v_mem_off++;
    }

    swap_table = PROCESS_HEAP_START + (head_offset << seL4_PageBits);
    swap_freehead = swap_table;
}

/* swap out */
extern int pin_num ;
static uint32_t swap_pointer = 1;
uint32_t swapout(sos_as *pcb, seL4_Uservaddr va){
    uint32_t ret;
	while(1)
	{
		//unmap 所有 frame,使用revoke函数
        sos_lock_acquire(pcb,MEM_LOCK);
		int err;
        seL4_CPtr sos_cap;
		if((!frame_check_pin(swap_pointer)) 
                && frame_check_alloc(swap_pointer))
		{
			if(frame_check_ref(swap_pointer))
            {
				unset_frame_ref(swap_pointer);
                sos_cap = frame_seek_sos_cap(swap_pointer);
				err = cspace_revoke_cap(cur_cspace, sos_cap);
			}
			else
            {
				ret = _swap_to_file(pcb,swap_pointer);
                if(ret)
                {
                    sos_lock_release(pcb,MEM_LOCK);
                    return -1;
                }

                ret = swap_pointer;
                frame_set_pcb(ret,pcb,va);
                set_frame_pin(ret);

                swap_pointer++;
                swap_pointer = swap_pointer % (FRAMETABLE_SIZE);
                if(swap_pointer == 0) swap_pointer = 1;
                sos_lock_release(pcb,MEM_LOCK);
				break;
			}
		}

        swap_pointer++;
		swap_pointer = swap_pointer % (FRAMETABLE_SIZE);
        if(swap_pointer == 0) swap_pointer = 1;
        sos_lock_release(pcb,MEM_LOCK);
	}
	return ret;
}

/* swapout的时候,使用frametable index反向找到shallow pagetable 的pageinfo, 更新信息 */
BOOL _update_pagetable_info(int swapfile_index,int frametable_index){
	//更新pagetable info
	seL4_Uservaddr va = frame_seek_Userpage(frametable_index);	
	sos_as *pcb = frame_seek_pcb(frametable_index);

	if(!check_alloc(pcb, va))
		return FALSE;

    pageinfo pi = get_pageinfo(pcb, va);

	pageinfo_setSwapFlag(pi);
	/* 更新swap table的phy id */
	uint32_t bit = 0xFC000000;
	swapfile_index &= (~bit);
	*pi &= bit;
	*pi |= swapfile_index;
	return TRUE;

}

static write_token swapout_token[4];
int _swap_to_file(sos_as* pcb,int frametable_index){
	//调用nfs_write,写到swap_file中一个空闲地方i,将这个i写回shallow pagetable
	int swapfile_index = swap_pop_free_node(pcb);
    seL4_Sosvaddr buffer = frame_seek_sos_addr(frametable_index);
	seL4_Uservaddr uvaddr = frame_seek_Userpage(frametable_index);

    dprintf(0,"SWAPOUT pid = %d user vaddr = %p\n",pcb -> pid ,uvaddr);

    for(int i = 0;i < 4;i ++){
        swapout_token[i].isready = 0;
        swapout_token[i].err = nfs_write(
                &(NFS_SWAP_FILE),
                swapfile_index * PAGE_SIZE + i * TOKEN_BUF_SIZ,
                TOKEN_BUF_SIZ,
                (buffer + TOKEN_BUF_SIZ * i),
                &_write_cb,
                (uintptr_t)(swapout_token + i));
    }

    if(swapout_token[0].err || swapout_token[1].err 
            || swapout_token[2].err || swapout_token[3].err)
    {
        dprintf(0,"Failed to swapout ,due to error : %d,%d,%d,%d\n",
                swapout_token[0].err , swapout_token[1].err,
                swapout_token[2].err ,swapout_token[3].err);
        return VFS_ERROR;
    }


    while(!(swapout_token[0].isready && swapout_token[1].isready 
                && swapout_token[2].isready && swapout_token[3].isready)){
        seL4_Yield();
	}

    /* check nfs status */
    if(swapout_token[0].status || swapout_token[1].status 
            || swapout_token[2].status || swapout_token[3].status)
    {
        dprintf(0,"Failed to swapout ,due to status: %d,%d,%d,%d\n",
                swapout_token[0].status ,swapout_token[1].status,
                swapout_token[2].status ,swapout_token[3].status);
        return VFS_ERROR;
    }

    _update_pagetable_info(swapfile_index,frametable_index);

    bzero(buffer,4096);

    return VFS_OK;
}

int
swap_pop_free_node(sos_as* pcb)
{
    union u_swap *pRet;

    sos_lock_acquire(pcb,SWAP_LOCK);
    pRet = swap_freehead;
	if(pRet->head.nextfree_ == NULL)
		swap_freehead = pRet + 1;
	else
		swap_freehead = pRet->head.nextfree_;
    sos_lock_release(pcb,SWAP_LOCK);
    return pRet - swap_table;
}

	void 
swap_recycle_node(sos_as* pcb,int index)
{
    sos_lock_acquire(pcb,SWAP_LOCK);
	union u_swap *node = (swap_table + index);
    node->head.nextfree_ = swap_freehead;
    swap_freehead = node;
    sos_lock_release(pcb,SWAP_LOCK);
}

	uint32_t
swap_get_file_offset(int index)
{
    return index * PAGE_SIZE;
}

int swap_file_init()
{
    /* only init once */
    static uint32_t i = 0;
    if(i > 0) return;
    i++;

    lookup_token* lup_token = (lookup_token*) malloc(sizeof(lookup_token));
    conditional_panic(!lup_token,"Failed to malloc swapfile token");
    memset(lup_token,0,sizeof(lookup_token));
    lup_token->inode = (nfs_inode *)malloc(sizeof(nfs_inode));
    conditional_panic(!(lup_token->inode),"Failed to malloc swapfile inode");
    memset(lup_token->inode,0,sizeof(nfs_inode));
   
    lup_token->isready = 0;
    enum rpc_stat err = nfs_lookup(&mnt_point, "swapfile",_lookup_or_create_cb, lup_token);
    while(lup_token->isready==0)
        seL4_Yield();

    if (lup_token->status == NFSERR_NOENT)
    {
        /* if file does not exit,create it */
        dprintf(0,"start create file\n");
        sattr_t sattr = {
            .mode = 0b110110110,
            .uid = 1,
            .gid = 1,
            .size = 0,
        };
        timestamp_t t = time_stamp()/1000;
        sattr.atime.seconds = _time_base + t;
        sattr.mtime.seconds = _time_base + t;
        lup_token->isready = 0;
        err = nfs_create(&mnt_point,"swapfile",&sattr,&_lookup_or_create_cb,lup_token);
        while(lup_token->isready == 0)
            seL4_Yield();

        /* return false if create file fail */
        if (err)
        {
            free(lup_token);
            return -1;
        }
    }

	memcpy(&NFS_SWAP_FILE, &(lup_token -> inode -> fh), sizeof(fhandle_t));
    free(lup_token->inode);
    free(lup_token);

    return 0;
}


/* swap in */

/* 检查page是否换出,1 换出,0 没有换出 */
uint32_t check_page_status(pageinfo pi){
	return ((*pi)& PAGE_SWAP_FLAG) >> 28;
}

uint32_t get_swaptable_index(pageinfo pi){
	return (*pi)&0x0007FFFF;
}

/* swap in的时候update pageinfo */
void _update_pageinfo(pageinfo pi, int index){
	uint32_t bit =  0xFC000000;
	*pi &= bit;
	index &= (~bit);
	*pi |= index;
	pageinfo_unsetSwapFlag(pi);
}

static read_token r_token[4];
int _swap_from_file(int id, int swaptable_index){
	seL4_Sosvaddr sa = frame_seek_sos_addr(id);
	for(int i = 0;i < 4 ;i ++)
    {
        r_token[i].isready = 0;
        r_token[i].data = (sa + TOKEN_BUF_SIZ * i);
        r_token[i].err = nfs_read(
                &(NFS_SWAP_FILE),
                swaptable_index * PAGE_SIZE + TOKEN_BUF_SIZ * i,
                TOKEN_BUF_SIZ,
                &_read_cb,
                (uintptr_t)(r_token + i));
    }

    if(r_token[0].err || r_token[1].err 
            || r_token[2].err || r_token[3].err)
    {
        dprintf(0,"Failed to swapin,due to error : %d,%d,%d,%d\n",
                r_token[0].err ,r_token[1].err, 
                r_token[2].err ,r_token[3].err);
        return VFS_ERROR;
    }


    while(!(r_token[0].isready && r_token[1].isready 
                && r_token[2].isready && r_token[3].isready))
        seL4_Yield();

    /* check nfs status */
    if(r_token[0].status || r_token[1].status 
            || r_token[2].status || r_token[3].status)
    {
        dprintf(0,"Failed to swapin,due to status: %d,%d,%d,%d\n",
                r_token[0].status ,r_token[1].status, 
                r_token[2].status ,r_token[3].status);
        return VFS_ERROR;
    }
	return VFS_OK;
}

/* 使用pageinfo找到换出去的页在swaptable的index */
int swapin(pageinfo pi, int swaptable_index, sos_as *pcb, seL4_Uservaddr va){
    int err;
	int id  = frame_alloc(pcb, va);
    if(id == 0)
        id = swapout(pcb,va);

    if(id < 1) return VFS_ERROR;

	err = _swap_from_file(id, swaptable_index);

    /* recycle swaptable id whenever swapin done*/
	swap_recycle_node(pcb,swaptable_index);

    if(err) return VFS_ERROR;

	_update_pageinfo(pi,id); 

    sos_lock_acquire(pcb,MEM_LOCK);
    pagetable_map_page(pcb,va);
    sos_lock_release(pcb,MEM_LOCK);

    return VFS_OK;
}

