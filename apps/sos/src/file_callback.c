#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <nfs/nfs.h>
#include <cspace/cspace.h>
#include <serial/serial.h>
#include "vfs.h"
#include "env.h"
#include "frametable.h"
#include "pcb.h"
#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include "vmem_layout.h"
#include "file.h"
#include "task.h"
#include "file_callback.h"

void _lookup_or_create_cb(uintptr_t token, enum nfs_stat status, fhandle_t *fh, fattr_t* fattr){
    lookup_token* ptoken = (lookup_token*) token;
    ptoken->status = status;

    memcpy(&(ptoken->inode->fh),fh,sizeof(fhandle_t));
    memcpy(&(ptoken->inode->fattr),fattr,sizeof(fattr_t));
    ptoken->isready = 1;
}

void _write_cb(uintptr_t token, enum nfs_stat status, fattr_t* fattr, int count){
	write_token *tk = (write_token *) token;
	tk -> status = status;
    tk -> count = count;
	tk -> isready = 1;
}

void _read_cb(uintptr_t token, enum nfs_stat status,fattr_t *fattr, int count, void* data)
{
	read_token *tk = (read_token *) token;
	tk -> status = status;

    tk -> count = count;
    if(count > 0)
        memcpy(tk->data,(void *)data,count);

	tk -> isready = 1;
}

void _readdir_cb(uintptr_t token, enum nfs_stat status,int num_files, char* file_names[],nfscookie_t nfscookie)
{
    readdir_token* tk = (readdir_token*)token;
    if(tk->history_num_files + num_files > tk-> pos)
    {
        int len;
        len = strlen(file_names[tk->pos - tk->history_num_files]);
        tk->name = malloc(len + 1);
        memcpy(tk->name,file_names[tk->pos - tk->history_num_files],len);
        tk->name[len] = 0;
    }

    tk->status = status;
    tk->history_num_files += num_files;
    tk->cookie = nfscookie;
    tk->isready = 1;
}
