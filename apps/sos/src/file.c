#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <nfs/nfs.h>
#include <cspace/cspace.h>
#include <serial/serial.h>
#include <clock/clock.h>
#include "vfs.h"
#include "env.h"
#include "frametable.h"
#include "swaptable.h"
#include "lock.h"
#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include "vmem_layout.h"
#include "file.h"
#include "task.h"
#include "file_callback.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define UNIX_TIME_BASE (2208988800L)
#define FILE_SUBTHREAD_BADGE    2000

extern fhandle_t mnt_point;
uint32_t _time_base;

static int _do_write(nfs_inode *inode,uint32_t *offset,seL4_Sosvaddr buf,size_t length);
static int _do_read(nfs_inode *inode,uint32_t *offset,seL4_Sosvaddr buf,size_t length);
static int file_close(sos_vfs_vnode* vnode);
static int file_open(seL4_File_Descripter local_id,seL4_File_Descripter *glb_id,sos_vfs_vnode **p_vnode,seL4_Buffer path,seL4_CPtr reply_cap);
static int file_read(sos_as *pcb,nfs_inode *inode,uint32_t *offset,seL4_Uservaddr buf,size_t length,seL4_CPtr reply_cap);
static int file_write(sos_as *pcb,nfs_inode *f_token,uint32_t *offset,seL4_Uservaddr ubuf,size_t length,seL4_CPtr reply_cap);
static int file_stat(seL4_Buffer path,seL4_CPtr reply_cap);
static int file_getdirent(int pos,seL4_CPtr reply_cap);

void 
time_worker(uint32_t id, void *data)
{
    nfs_timeout();
    sync_acquire(lock_arr[TIME_LOCK]) ;
    register_timer(100, time_worker, NULL);
    sync_release(lock_arr[TIME_LOCK]) ;
}

int 
sos_file_init(struct sos_vfs_operation* op,seL4_CPtr sos_ep)
{
    /* sanity check */
    if(!op)
        return VFS_ERROR;
    op->vop_open = &file_open;
    op->vop_read = &file_read;
    op->vop_write= &file_write;
    op->vop_close = &file_close;

    op->vop_stat = &file_stat;
    op->vop_getdirent = &file_getdirent;

    /* init file time infor */
    struct ip_addr udpserver;
    ipaddr_aton("192.168.1.1",&udpserver);
    _time_base = udp_time_get(&udpserver) - UNIX_TIME_BASE;

    sync_acquire(lock_arr[TIME_LOCK]) ;
    register_timer(100, time_worker, NULL);
    sync_release(lock_arr[TIME_LOCK]) ;
}

static int file_close(sos_vfs_vnode* vnode)
{

}

static int 
file_open(seL4_File_Descripter local_id,
        seL4_File_Descripter *glb_id,
        sos_vfs_vnode **p_vnode,
        seL4_Buffer path,
        seL4_CPtr reply_cap)
{
    if(strlen(path) > MAXPATHLEN )
    {
        dprintf(0,"Failed to open file,path length is %d greater than %d\n",strlen(path),MAXPATHLEN);
        free(path);
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }

    char search = '/';
    char* sub_path = path;
    char * name;
    while(name = strchr(sub_path,search))
        sub_path = name;

    if(strlen(sub_path) > MAXNAMLEN)
    {
        dprintf(0,"Failed to open file,name length is %d greater than %d\n",strlen(sub_path),MAXNAMLEN);
        free(path);
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }

    lookup_token* lup_token = (lookup_token*) malloc(sizeof(lookup_token));
    if(!lup_token)
    {
        dprintf(0,"Failed in file_open : due to malloc lookup token faile\n");
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }
    memset(lup_token,0,sizeof(lookup_token));

    lup_token->inode = (nfs_inode *) malloc(sizeof(nfs_inode));
    if(!(lup_token->inode))
    {
        dprintf(0,"Failed in file_open  due to malloc inode faile\n");
        free(lup_token);
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }

    memset(lup_token->inode,0,sizeof(nfs_inode));
    lup_token->isready = 0;
    enum rpc_stat err = nfs_lookup(&mnt_point, path,_lookup_or_create_cb, lup_token);
    while(lup_token->isready==0)
        seL4_Yield();

    /* if file does not exit,create it */
    if (lup_token->status == NFSERR_NOENT)
    {
        sattr_t sattr = {
            .mode = 0b110110110,
            .uid = 1,
            .gid = 1,
            .size = 0,
        };
        timestamp_t t = time_stamp()/1000;
        sattr.atime.seconds = _time_base + t;
        sattr.mtime.seconds = _time_base + t;
        lup_token->isready = 0;
        err = nfs_create(&mnt_point,path,&sattr,&_lookup_or_create_cb,lup_token);
        while(lup_token->isready == 0)
            seL4_Yield();

        /* return false if create file fail */
        if (err)
        {
            free(path);
            free(lup_token->inode);
            free(lup_token);
            free(*p_vnode);
            *p_vnode = NULL;
            *glb_id = -1;
            sos_reply_cap(reply_cap,-1);
            return VFS_ERROR;
        }
    }
    //创建vnode 把token 直接存入，token中带有文件访问handler 以及文件属性描述符
    (*p_vnode)->inode = lup_token->inode;

    err = 0;
    if(!(lup_token -> inode -> fattr.mode & 0b100100100))
    {
        if(FM_READ & (*p_vnode) -> permission)
            err |= 1;
    }
    if(!(lup_token -> inode -> fattr.mode & 0b010010010))
    {
        if(FM_WRITE & (*p_vnode) -> permission)
            err |= 1;
    }
    if(!(lup_token -> inode -> fattr.mode & 0b001001001))
    {
        if(FM_EXEC & (*p_vnode) -> permission)
            err |= 1;
    }
    if (err)
    {
        dprintf(0,"Failed to open file,due to permission err \n");
        free(path);
        free(lup_token->inode);
        free(lup_token);
        free(*p_vnode);
        *p_vnode = NULL;
        *glb_id = -1;
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }

    free(lup_token);
    free(path);

    sos_reply_cap(reply_cap,local_id);

    return 0;
}

static int file_read(sos_as *pcb,
        nfs_inode *inode,
        uint32_t *offset,
        seL4_Uservaddr buf,
        size_t length,
        seL4_CPtr reply_cap)
{
    int err;
    seL4_Sosvaddr map_buf;
    int count = 0;

    while(length)
    {
        size_t seglen;
        int ret;
        seglen = MIN(length,PAGE_SIZE - ((uint32_t)(buf) & PAGE_OFFSET_FRAME));

        as_region* region = sos_pcb_seek_region(pcb,buf);
        if(region == NULL 
                && region != pcb -> _heap 
                && region != pcb -> _stack)
        {
            dprintf(0,"Segment Fault in file read\n");
            if(reply_cap)
                sos_reply_cap(reply_cap,-1);
            return VFS_ERROR;
        }

        if(!(seL4_CanWrite & pagetable_seekpermission(pcb,buf)))
        {
            dprintf(0,"Failed to read,memory permission cannot write\n");
            if(reply_cap)
                sos_reply_cap(reply_cap,-1);
            return VFS_ERROR;
        }

        if(check_alloc(pcb,buf))
        {
            sos_lock_acquire(pcb,MEM_LOCK);
            pageinfo pi = get_pageinfo(pcb, buf);
            if(check_page_status(pi) == SWAPOUT)
            {
                sos_lock_release(pcb,MEM_LOCK);
                int swaptable_index = get_swaptable_index(pi);
                err = swapin(pi, swaptable_index, pcb, buf);
                if(err)
                {
                    dprintf(0,"Failed in file_read : process = %d, due to swapin err\n",pcb -> pid);
                    sos_reply_cap(reply_cap,-1);
                    return VFS_ERROR;
                }
            }
            else
            {
                pagetable_pin(pcb,buf);
                sos_lock_release(pcb,MEM_LOCK);
            }
        }
        else
        {
            err = pagetable_alloc(pcb,buf & PAGE_FRAME,seL4_CanRead|seL4_CanWrite,seL4_ARM_Default_VMAttributes);
            if(err)
            {
                dprintf(0,"Failed in file_read :process = %d, due to pagetable alloc err\n",pcb -> pid);
                sos_reply_cap(reply_cap,-1);
                return VFS_ERROR;
            }
        }

        pagetable_switch2sosaddr(pcb,buf,&map_buf);

        ret = _do_read(inode,offset,map_buf,seglen);

        pagetable_unpin(pcb,buf);

        if(ret > 0)
        {
            count += ret;
            length -= ret;
            buf += ret;
        }
        else if(ret == 0)
        {
            break;
        }
        else if(ret < 0)
        {
            dprintf(0,"Failed do nfs read \n");
            sos_reply_cap(reply_cap,-1);
            return VFS_ERROR;
        }
    }
    sos_reply_cap(reply_cap,count);
}

static read_token r_token[4] ;
static int 
_do_read(nfs_inode *inode,
        uint32_t *offset,
        seL4_Sosvaddr buf,
        size_t length)
{
    int read_thd_count = 0;

    for(int i = 0;i < 4 ;i ++)
    {
        if(TOKEN_BUF_SIZ * i < length)
        {
            r_token[i].isready = 0;
            r_token[i].data = (buf + TOKEN_BUF_SIZ * i);
            r_token[i].err = nfs_read(
                    &(inode -> fh),
                    *offset + TOKEN_BUF_SIZ * i,
                    MIN(TOKEN_BUF_SIZ,length - TOKEN_BUF_SIZ * i),
                    &_read_cb,
                    (uintptr_t)(r_token + i));
        }
        else
        {
            r_token[i].isready = 1;
            r_token[i].count = 0;
            r_token[i].err = 0;
            r_token[i].status = 0;
        }
    }

    if(r_token[0].err || r_token[1].err 
            || r_token[2].err || r_token[3].err)
    {
        return -1;
    }


    while(!(r_token[0].isready && r_token[1].isready 
                && r_token[2].isready && r_token[3].isready))
    {
        seL4_Yield();
    }

    if(r_token[0].status || r_token[1].status 
            || r_token[2].status || r_token[3].status)
    {
        return -1;
    }

    for(int i = 0;i < 4 ;i ++)
    {
        read_thd_count += r_token[i].count;
    }

    
    *offset += read_thd_count;

    return read_thd_count;

}

file_write(sos_as *pcb,
        nfs_inode *inode,
        uint32_t *offset,
        seL4_Uservaddr ubuf,
        size_t length,
        seL4_CPtr reply_cap)
{
    int err,ret;
    size_t seglen,send_count;
    seL4_Sosvaddr map_buf;
    send_count = 0;
    while(length)
    {
        seglen = MIN(length,PAGE_SIZE - (ubuf & PAGE_OFFSET_FRAME));

        as_region* region = sos_pcb_seek_region(pcb,ubuf);
        if(region == NULL 
                && region != pcb -> _heap 
                && region != pcb -> _stack)
        {
            dprintf(0,"Segment Fault in file write\n");
            if(reply_cap)
                sos_reply_cap(reply_cap,-1);
            return VFS_ERROR;
        }

        if(check_alloc(pcb,ubuf))
        {
            sos_lock_acquire(pcb,MEM_LOCK);
            pageinfo pi = get_pageinfo(pcb, ubuf);
            if(check_page_status(pi) == SWAPOUT)
            {
                sos_lock_release(pcb,MEM_LOCK);
                int swaptable_index = get_swaptable_index(pi);
                err = swapin(pi, swaptable_index, pcb, ubuf);
                if(err)
                {
                    dprintf(0,"Failed in file_write :process = %d, due to swapin err\n",pcb -> pid);
                    sos_reply_cap(reply_cap,-1);
                    return VFS_ERROR;
                }

            }
            else
            {
                pagetable_pin(pcb,ubuf);
                sos_lock_release(pcb,MEM_LOCK);
            }
        }
        else
        {
            err = pagetable_alloc(pcb,ubuf & PAGE_FRAME,seL4_CanRead|seL4_CanWrite,seL4_ARM_Default_VMAttributes);
            if(err)
            {
                dprintf(0,"Failed in con_write:process = %d, due to pagetable alloc err\n",pcb -> pid);
                sos_reply_cap(reply_cap,-1);
                return VFS_ERROR;
            }
        }

        pagetable_switch2sosaddr(pcb,ubuf,&map_buf);

        ret = _do_write(inode,offset,map_buf,seglen);
        pagetable_unpin(pcb,ubuf);

        if(ret == -1)
        {
            if(reply_cap)
                sos_reply_cap(reply_cap,-1);
            return -1;
        }
        if(ret == 0)
            break;

        send_count += ret;
        length -= seglen;
        ubuf += seglen;
    }

    if(reply_cap)
        sos_reply_cap(reply_cap,send_count);

    return 0;
}
static write_token w_token[4];
static int 
_do_write(nfs_inode *inode,
        uint32_t *offset,
        seL4_Sosvaddr buf,
        size_t length)
{
    uint32_t write_thd_count = 0;
    for(int i = 0;i < 4;i ++)
    {
        if(TOKEN_BUF_SIZ * i < length)
        {
            w_token[i].isready = 0;
            w_token[i].err = nfs_write(
                    &(inode -> fh),
                    *offset + TOKEN_BUF_SIZ * i,
                    MIN(TOKEN_BUF_SIZ,length - TOKEN_BUF_SIZ * i),
                    (buf + TOKEN_BUF_SIZ * i),
                    &_write_cb,
                    (uintptr_t)(w_token + i));
        }
        else
        {
            w_token[i].isready = 1;
            w_token[i].count = 0;
            w_token[i].err = 0;
            w_token[i].status = 0;
        }
    }

    /* check rpc err */
    if(w_token[0].err || w_token[1].err 
            || w_token[2].err || w_token[3].err)
        return -1;

    while(!(w_token[0].isready && w_token[1].isready 
                && w_token[2].isready && w_token[3].isready))
        seL4_Yield();

    /* check nfs status */
    if(w_token[0].status || w_token[1].status 
            || w_token[2].status || w_token[3].status)
        return -1;

    for(int i = 0;i < 4 ;i ++)
    {
        write_thd_count += w_token[i].count;
        *offset += w_token[i].count;
    }

    return write_thd_count;
}

static int file_stat(seL4_Buffer path,seL4_CPtr reply_cap)
{
    lookup_token* lup_token = (lookup_token*) malloc(sizeof(lookup_token));
    if(!lup_token)
    {
        free(path);
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }
    memset(lup_token,0,sizeof(lookup_token));
    lup_token->inode = (nfs_inode *) malloc(sizeof(nfs_inode));
    if(!(lup_token->inode))
    {
        free(path);
        free(lup_token);
        sos_reply_cap(reply_cap,-1);
        return VFS_ERROR;
    }
    memset(lup_token->inode,0,sizeof(nfs_inode));
    lup_token->isready = 0;

    enum rpc_stat err = nfs_lookup(&mnt_point, path,_lookup_or_create_cb, lup_token);
    while(lup_token->isready==0)
        seL4_Yield();


    if (!(lup_token->status) && !err)
    {
        uint32_t fmode = 0;
        seL4_MessageInfo_t reply = seL4_MessageInfo_new(0, 0, 0, 6);
        seL4_SetMR(0,0);
        seL4_SetMR(1,lup_token->inode->fattr.type);
        if(lup_token -> inode -> fattr.mode & 0b100100100)
            fmode |= FM_READ;
        if(lup_token -> inode -> fattr.mode & 0b010010010)
            fmode |= FM_WRITE;
        if(lup_token -> inode -> fattr.mode & 0b001001001)
            fmode |= FM_EXEC;
        seL4_SetMR(2,fmode);
        seL4_SetMR(3,lup_token->inode->fattr.size);
        seL4_SetMR(4,lup_token->inode->fattr.ctime.seconds);
        seL4_SetMR(5,lup_token->inode->fattr.atime.seconds);
        seL4_Send(reply_cap, reply);
        /* Free the saved reply cap */
        cspace_free_slot(cur_cspace, reply_cap);
    }
    else
    {
        sos_reply_cap(reply_cap,-1);
    }

    free(lup_token->inode);
    free(lup_token);
    free(path);
    return 0;
}

static int file_getdirent(int pos,seL4_CPtr reply_cap)
{

    int err;
    if (pos < 0)
    {
        dprintf(0,"Failed to getdirent,pos is = %d\n",pos);
        sos_reply_cap(reply_cap,-1);
        return;
    }

    readdir_token* token = malloc(sizeof(readdir_token));
    if(!token)
        sos_reply_cap(reply_cap,-1);

    memset(token,0,sizeof(readdir_token));
    token->pos = pos;

    do
    {
        token->isready = 0;
        err = nfs_readdir(&mnt_point, token->cookie, &_readdir_cb,
                (uintptr_t)token);
        while(token->isready == 0)
            seL4_Yield();

        if(err || token->cookie == 0)
            break;
    }
    while(token->history_num_files < pos);


    seL4_MessageInfo_t reply = seL4_MessageInfo_new(0, 0, 0, seL4_MsgMaxLength);
    if(token->name)
    {
        int len;
        seL4_SetMR(0,0);
        len = strlen(token->name);
        seL4_SetMR(1,len);
        strncpy((char*)(seL4_GetIPCBuffer()->msg + 2),token->name,len);
        free(token->name);

        seL4_Send(reply_cap, reply);
    }
    else if(!err && !(token->status)&& token->pos== token->history_num_files)
    {
        //scan complete
        seL4_SetMR(0,0);
        seL4_SetMR(1,0);
        seL4_SetMR(2,0);
        seL4_Send(reply_cap, reply);
    }
    else
    {
        seL4_SetMR(0,-1);
        seL4_Send(reply_cap, reply);
    }

    cspace_free_slot(cur_cspace, reply_cap);

    free(token);
    return 0;
}
