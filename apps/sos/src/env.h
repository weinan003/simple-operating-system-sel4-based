#ifndef __ENV_MACRO__
#define __ENV_MACRO__
#include <cspace/cspace.h>
#include <sel4/sel4.h>
#define VFS_OK 0
#define VFS_ERROR 1;

#define O_ACCMODE 03
#define O_RDONLY  00
#define O_WRONLY  01
#define O_RDWR    02

/* This is the index where a clients syscall enpoint will
 * be stored in the clients cspace. */
#define USER_EP_CAP          (1)
/* To differencient between async and and sync IPC, we assign a
 * badge to the async endpoint. The badge that we receive will
 * be the bitwise 'OR' of the async endpoint badge and the badges
 * of all pending notifications. */
#define IRQ_EP_BADGE         (1 << (seL4_BadgeBits - 1))
/* All badged IRQs set high bet, then we use uniq bits to
 * distinguish interrupt sources */
#define IRQ_BADGE_NETWORK (1 << 0)
#define IRQ_BADGE_CLOCK (1 << 1)

#define TTY_NAME             CONFIG_SOS_STARTUP_APP
#define TTY_PRIORITY         (0)
#define TTY_EP_BADGE         (101)

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif
#define SWAPOUT 1 //表示page被换出

typedef int BOOL;
typedef char* seL4_Buffer;    
typedef const char* seL4_Const_Buffer;

typedef seL4_Word   seL4_ID;
typedef seL4_CPtr   seL4_Uservaddr;
typedef seL4_CPtr   seL4_Sosvaddr;
typedef seL4_CPtr   seL4_EndpointCap;
typedef seL4_Int32  seL4_File_Descripter;

void sos_reply_cap(seL4_CPtr reply_cap,int ret);
#endif
