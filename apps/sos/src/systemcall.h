#ifndef __SYSTEM_CALL__
#define __SYSTEM_CALL__
#include <cspace/cspace.h>
#include <sel4/sel4.h>
struct sos_as;

/* sub thread access point create application in this function */
void wrapper_system_loop();

/* hardware IRQ handle loop */
void systemcall_IRQ_loop();

/* sos work thread serve for application */
void systemcall_loop(sos_as *pcb);
/* work thread systemcall handler */
void systemcall_handler(sos_as *pcb, int num_args);

/* sos main thread msg loop, manage process */
void systemcall_sos_loop(seL4_CPtr ep);

void systemcall_register_sleep(sos_as *pcb,seL4_CPtr reply_cap);
void systemcall_time_stamp(seL4_CPtr reply_cap);
void systemcall_open(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_close(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_read(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_write(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_stat(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_getdirent(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_brk(sos_as* pcb,seL4_CPtr reply_cap);

void systemcall_proc_create(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_proc_delete(sos_as* pcb,seL4_CPtr reply_cap);
void systemcall_proc_status(sos_as* pcb,seL4_CPtr reply_cap);

#endif
