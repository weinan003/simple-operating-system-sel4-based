#ifndef __FILE_H__
#define __FILE_H__
#include<cspace/cspace.h>

#define OPEN_TASK       (1)
#define READ_TASK       (2)
#define WRITE_TASK      (3)
#define STAT_TASK       (4)
#define GEDIENT_TASK    (5)

#define TASK_NUM        (2)
#define TOKEN_BUF_SIZ   (1024)
struct sos_vfs_operation;

int sos_file_init(struct sos_vfs_operation* op,seL4_CPtr sos_ep);

void sos_file_subthread_call(seL4_CPtr reply_cap);
#endif
