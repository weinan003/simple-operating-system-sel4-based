#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <cspace/cspace.h>

#include "lock.h"
#include "vmem_layout.h"
#include "pcb.h"

#include <autoconf.h>
#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>

int lock_init()
{
    for(int i = 1;i < LOCK_NUM;i ++)
    {
        lock_arr[i] = sync_create_mutex(); 
        if(!(lock_arr[i])) return i;
    }
    return 0;
}

void 
sos_lock_acquire(sos_as* pcb,int lock_id)
{
    sync_acquire(lock_arr[lock_id]);
    pcb->hold_lock += 1;
}

extern msg_queue *proc_q;
void 
sos_lock_release(sos_as* pcb,int lock_id)
{
    sync_release(lock_arr[lock_id]);
    pcb->hold_lock -= 1;
    if(pcb->async_suspend && pcb->hold_lock == 0)
    {
        seL4_TCB_Suspend(pcb->tcb_cap);
        _msg_queue_push_back(proc_q,pcb);
    }
}
