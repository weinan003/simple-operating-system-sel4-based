#ifndef __LOCK_H__
#define __LOCK_H__
#include <sync/mutex.h>
typedef struct sos_as sos_as;

#define SWAP_LOCK   (1)
#define MEM_LOCK    (2)
#define TIME_LOCK   (3)
#define PROC_LOCK   (4)
#define VFS_LOCK    (5)
#define LOCK_NUM    (6)

/* 0 lock slot not use*/
sync_mutex_t lock_arr[LOCK_NUM];

int lock_init();
void sos_lock_acquire(sos_as* pcb,int lock_id);
void sos_lock_release(sos_as* pcb,int lock_id);
#endif
