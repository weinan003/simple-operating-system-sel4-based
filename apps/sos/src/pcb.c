#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <cspace/cspace.h>

#include <clock/clock.h>
#include <nfs/nfs.h>
#include <elf/elf.h>
#include <serial/serial.h>

#include "network.h"
#include "elf.h"

#include "ut_manager/ut.h"
#include "vmem_layout.h"
#include "mapping.h"
#include "pagetable.h"
#include "frametable.h"
#include "console.h"
#include "pcb.h"
#include "env.h"
#include "vfs.h"
#include "systemcall.h"
#include "file.h"
#include "file_callback.h"
#include "lock.h"
#include <autoconf.h>

#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>

#define PAGESIZE              (1 << (seL4_PageBits))
#define PAGEMASK              ((PAGESIZE) - 1)
#define PAGE_ALIGN(addr)      ((addr) & ~(PAGEMASK))

extern fhandle_t mnt_point ;

static nfs_inode* init_elf_handler(char *app_name)
{
    nfs_inode* ret = NULL;
    lookup_token* lup_token = (lookup_token*) malloc(sizeof(lookup_token));
    if(!lup_token) 
        return ret;
    memset(lup_token,0,sizeof(lookup_token));

    lup_token->inode = (nfs_inode *)malloc(sizeof(nfs_inode));
    if(!(lup_token->inode))
    {
        free(lup_token);
        return ret;
    }
    memset(lup_token->inode,0,sizeof(nfs_inode));

    nfs_lookup(&mnt_point,app_name,_lookup_or_create_cb,lup_token);

    while(lup_token->isready==0)
        seL4_Yield();

    if (lup_token->status)
    {
        free(lup_token->inode);
        free(lup_token);
        return ret;
    }

    ret = lup_token->inode;
    free(lup_token);

    return ret;
}

//没有删塞如特，删了会有霸个，exec两次没有的文件内核会挂
static inline void _pcb_destory_space(sos_as *pcb)
{
    pagetable_free(pcb);
    seL4_ARM_Page_Unmap(pcb->vroot);
    cspace_delete_cap(cur_cspace,pcb->vroot);
    ut_free(pcb->vroot_addr,seL4_PageDirBits);
}

static inline void 
_pcb_destory_tcb(sos_as *pcb)
{
    cspace_delete_cap(cur_cspace,pcb->tcb_cap);
    ut_free(pcb->tcb_addr,seL4_TCBBits);
}

static void 
destory_region(sos_as *pcb,as_region* region)
{
    if(region ->vbegin == region ->vend)
        return;

    region->vend -= 0x1000;
    while(region ->vbegin <= region->vend)
    {
        sos_lock_acquire(pcb,MEM_LOCK);
        if(check_alloc(pcb, region->vend))
        {
            pageinfo pi = get_pageinfo(pcb,region->vend);
            if(check_page_status(pi) == SWAPOUT)
            {
                int swaptable_index = get_swaptable_index(pi);

                swap_recycle_node(pcb,swaptable_index);
                sos_lock_release(pcb,MEM_LOCK);
            }
            else
            {
                pagetable_pin(pcb,region->vend);
                sos_lock_release(pcb,MEM_LOCK);

                pagetable_free_page(pcb,region->vend);
            }
        }
        else
            sos_lock_release(pcb,MEM_LOCK);

        region->vend -= 0x1000;
    }
}

int create_application(sos_as *pcb) {
    int err;
    char* app_name = pcb->name;
    seL4_CPtr fault_ep = pcb->ipc_ep;

    /* These required for setting up the TCB */
    seL4_UserContext context;

    /* These required for loading program sections */
    char elf_base[PAGE_SIZE];
    char* base;
    unsigned long elf_size;

    err = sos_pcb_init_space(pcb);
    if(err) return err;
    err = sos_pcb_init_tcb(pcb,fault_ep);
    if(err) 
    {
        dprintf(0,"Failed init tcb\n");
        _pcb_destory_space(pcb);
        return err;
    }

    as_region *stack = (as_region *) malloc(sizeof(as_region));
    if(!stack) 
    {
        dprintf(0,"Failed init stack\n");
        _pcb_destory_space(pcb);
        _pcb_destory_tcb(pcb);
        return !0;
    }
    stack->vbegin = PROCESS_STACK_TOP;
    stack->vend = PROCESS_STACK_TOP;
    pcb->_stack = stack;

    pcb->stime = time_stamp();

    as_region *heap = (as_region *) malloc(sizeof(as_region));
    if(!heap) 
    {
        dprintf(0,"Failed init heap\n");
        free(pcb->_stack);
        _pcb_destory_space(pcb);
        _pcb_destory_tcb(pcb);
        return !0;
    }
    heap->vbegin = PROCESS_HEAP_START;
    heap->vend = PROCESS_HEAP_START;
    pcb->_heap = heap;

    for(int i = 0;i < AS_FD_MAX;i ++)
        pcb->fd_table[i] = -1;

    pcb->proc_inode = init_elf_handler(app_name);
    if(!(pcb->proc_inode)) 
    {
        dprintf(0,"Failed init elf handler\n");
        free(pcb -> _heap);
        free(pcb -> _stack);

        L2_recordnode* pt = pcb->pt_recordlst;
        L2_recordnode* tmp = pt->next_;
        while(tmp)
        {
            seL4_ARM_Page_Unmap(tmp->pt_cap);
            cspace_delete_cap(cur_cspace,tmp->pt_cap);
            ut_free(tmp->uaddr,seL4_PageTableBits);

            pt->next_ = tmp ->next_;
            free(tmp);
            tmp = pt->next_;
        }
        free(pt);

        _pcb_destory_space(pcb);
        _pcb_destory_tcb(pcb);
        return !0;
    }

    pcb->p_size = pcb->proc_inode->fattr.size;

    dprintf(0, "\nStarting \"%s\"...\n", app_name);
    memset(elf_base,0,PAGE_SIZE);
    elf_load_region(pcb,elf_base,0,PAGE_SIZE);
    
    /* load the elf image */
    err = elf_load(pcb, elf_base);
    if(err) 
    {
        dprintf(0, "elf file %s load err = %d\n",app_name,err);
        free(pcb->proc_inode);
        free(pcb->_heap);
        free(pcb->_stack);
        if(err == seL4_RangeError)
        {
            as_region *r = pcb->_region;
            while(r)
            {
                as_region *tmp;
                destory_region(pcb,r);
                tmp = r->next_;
                free(r);
                r = tmp;
            }
        }

        L2_recordnode* pt = pcb->pt_recordlst;
        L2_recordnode* tmp = pt->next_;
        while(tmp)
        {
            seL4_ARM_Page_Unmap(tmp->pt_cap);
            cspace_delete_cap(cur_cspace,tmp->pt_cap);
            ut_free(tmp->uaddr,seL4_PageTableBits);

            pt->next_ = tmp ->next_;
            free(tmp);
            tmp = pt->next_;
        }
        free(pt);

        _pcb_destory_space(pcb);
        _pcb_destory_tcb(pcb);
        return !0;
    }

    sos_vfs_open(pcb,1,"console",FM_WRITE,0);
    sos_vfs_open(pcb,2,"console",FM_WRITE,0);

    /* Start the new process */
    memset(&context, 0, sizeof(context));
    context.pc = elf_getEntryPoint(elf_base);
    context.sp = PROCESS_STACK_TOP;
    seL4_TCB_WriteRegisters(pcb->tcb_cap, 1, 0, 2, &context);
    return 0;
}

void destroy_application(sos_as *pcb)
{
    dprintf(0,"start application delete\n");
    /* delete vnode */
    for(int i = 0;i < AS_FD_MAX;i ++)
        sos_vfs_close(pcb,i,NULL);
    dprintf(6,"close vfs fd\n");

    pagetable_free_page(pcb,PROCESS_IPC_BUFFER);

    /* free heap */
    destory_region(pcb,pcb->_heap);
    free(pcb->_heap);
    dprintf(6,"destory heap region\n");

    /* free stack */
    destory_region(pcb,pcb->_stack);
    free(pcb->_stack);
    dprintf(6,"destory stack region\n");

    /* free code region */
    as_region *r = pcb->_region;
    while(r)
    {
        dprintf(6,"destory region begin = %p,end = %p \n",r->vbegin,r->vend);
        as_region *tmp;
        destory_region(pcb,r);
        tmp = r->next_;
        free(r);
        r = tmp;
    }

    dprintf(6,"destory region finish\n");

    /* delete shadow pagetable */
    pagetable_free(pcb);

    dprintf(6,"destory shadow pagetable down\n");
    /* kernel pagetable tab free */
    L2_recordnode* pt = pcb->pt_recordlst;
    L2_recordnode* tmp = pt->next_;
    while(tmp)
    {
        seL4_ARM_Page_Unmap(tmp->pt_cap);
        cspace_delete_cap(cur_cspace,tmp->pt_cap);
        ut_free(tmp->uaddr,seL4_PageTableBits);

        pt->next_ = tmp ->next_;
        free(tmp);
        tmp = pt->next_;
    }
    free(pt);

    dprintf(6,"destory shadow pagetable record down\n");

    /* kernel pagetable dir free */
    seL4_ARM_Page_Unmap(pcb->vroot);
    cspace_delete_cap(cur_cspace,pcb->vroot);
    ut_free(pcb->vroot_addr,seL4_PageDirBits);

    dprintf(6,"destory vroot down\n");
    /* tcb free*/
    cspace_delete_cap(cur_cspace,pcb->tcb_cap);
    ut_free(pcb->tcb_addr,seL4_TCBBits);

    dprintf(6,"destory tcb down\n");
    /* ipc endpoint free*/
    cspace_delete_cap(cur_cspace,pcb->ipc_ep);
    ut_free(pcb->ipc_ut_addr,seL4_EndpointBits);

    dprintf(6,"destory ipc down\n");
    /* cspace free*/
    cspace_destroy(pcb->croot);
    dprintf(6,"destory croot down\n");

    while(pcb->wait_lst.next_)
    {
        wait_list* p = pcb->wait_lst.next_;

        if(p->pcb)
        {
            dprintf(0,"notify pid = %d wake up\n",p->pcb->pid);
            sos_reply_cap(p->pcb->reply_cap,pcb->pid);
            p->pcb->wait_backup = NULL;
            p->pcb->reply_cap = 0;
        }

        pcb->wait_lst.next_ = pcb->wait_lst.next_ ->next_;
        free(p);
    }

    while(glb_wait_lst.next_)
    {
        wait_list* p = glb_wait_lst.next_;

        if(p->pcb)
        {
            dprintf(0,"notify pid = %d wake up\n",p->pcb->pid);
            sos_reply_cap(p->pcb->reply_cap,pcb->pid);
            p->pcb->wait_backup = NULL;
            p->pcb->reply_cap = 0;
        }

        glb_wait_lst.next_ = glb_wait_lst.next_ ->next_;
        free(p);
    }

}

int sos_pcb_init_space(struct sos_as* pcb)
{
    /* Vspace init */
    int err;
    pcb->vroot_addr = ut_alloc(seL4_PageDirBits);
    if(!(pcb->vroot_addr)) 
        return !0;

    err = cspace_ut_retype_addr(pcb->vroot_addr,
            seL4_ARM_PageDirectoryObject,
            seL4_PageDirBits,
            cur_cspace,
            &(pcb->vroot));
    if(err)
    {
        ut_free(pcb->vroot_addr,seL4_PageDirBits);
        pcb -> vroot_addr = NULL;
        pcb -> vroot = NULL;
        return !0;
    }

    //Cspace init
    pcb->croot = cspace_create(1);
    if(pcb->croot == NULL)
    {
        cspace_delete_cap(cur_cspace,pcb->vroot);
        ut_free(pcb->vroot_addr,seL4_PageDirBits);
        pcb -> vroot_addr = NULL;
        pcb -> vroot = NULL;
        return !0;
    }

    //pagetable
    err = pagetable_init(pcb);
    if( err )
    {
        cspace_delete_cap(cur_cspace,pcb->vroot);
        ut_free(pcb->vroot_addr,seL4_PageDirBits);
        cspace_destroy(pcb -> croot);
        pcb -> croot = NULL;
        pcb -> vroot_addr = NULL;
        pcb -> vroot = NULL;
        return !0;
    }
}

int sos_pcb_init_tcb(struct sos_as* pcb,seL4_CPtr fault_ep)
{
    int err ;
    seL4_CPtr ipc_cap;
    seL4_CPtr user_ep_cap;

    err = pagetable_alloc(pcb,PROCESS_IPC_BUFFER,seL4_CanRead | seL4_CanWrite,seL4_ARM_Default_VMAttributes);

    if(err) 
    {
        dprintf(0,"Failed to pagetable_alloc an IPC buffer\n");
        return err;
    }
    pagetable_switch2soscap(pcb,PROCESS_IPC_BUFFER,&ipc_cap );

    sos_lock_acquire(pcb,MEM_LOCK);
    err = pagetable_map_page(pcb,PROCESS_IPC_BUFFER);
    sos_lock_release(pcb,MEM_LOCK);

    if (err ) 
    {
        dprintf(0,"Failed to map IPC buffer\n");
        pagetable_free_page(pcb,PROCESS_IPC_BUFFER);
        return err;
    }
    /* Copy the fault endpoint to the user app to enable IPC */
    user_ep_cap = cspace_mint_cap(pcb->croot,
            cur_cspace,
            fault_ep,
            seL4_AllRights,
            seL4_CapData_Badge_new(TTY_EP_BADGE));
    /* should be the first slot in the space, hack I know */
    assert(user_ep_cap == 1);
    assert(user_ep_cap == USER_EP_CAP);

    /* Create a new TCB object */
    pcb->tcb_addr = ut_alloc(seL4_TCBBits);
    if(!(pcb->tcb_addr)) 
    {
        dprintf(0,"Failed to alloc tcb \n");
        pagetable_free_page(pcb,PROCESS_IPC_BUFFER);
        return !0;
    }
    err =  cspace_ut_retype_addr(pcb->tcb_addr,
            seL4_TCBObject,
            seL4_TCBBits,
            cur_cspace,
            &(pcb->tcb_cap));
    if(err)
    {
        dprintf(0,"Failed to retype tcb addr\n");
        pagetable_free_page(pcb,PROCESS_IPC_BUFFER);
        ut_free(pcb->tcb_addr,seL4_TCBBits);
        return err;
    }

    /* Configure the TCB */
    err = seL4_TCB_Configure(pcb->tcb_cap, user_ep_cap, 255,
            pcb->croot->root_cnode, seL4_NilData,
            pcb->vroot, seL4_NilData, PROCESS_IPC_BUFFER,
            ipc_cap);
    if(err) 
    {
        dprintf(0,"Failed to configure application tcb\n");
        pagetable_free_page(pcb,PROCESS_IPC_BUFFER);
        ut_free(pcb->tcb_addr,seL4_TCBBits);
        return err;
    }

}

void sos_pcb_add_region(sos_as *pcb,as_region *region)
{
    if(!(pcb->_region))
    {
        pcb->_region = region;
    }
    else
    {
        region->next_ = pcb->_region;
        pcb->_region = region;
    }
}

as_region* sos_pcb_seek_region(sos_as *pcb,seL4_Uservaddr vaddr)
{
    if((pcb->_stack->vbegin - pcb->_heap->vend) == PAGE_SIZE && 
            (vaddr < pcb->_stack->vbegin) &&
            (pcb->_heap->vend <= vaddr))
        return NULL;

    /* check heap */
    if(pcb->_heap->vbegin <= vaddr && vaddr < pcb->_heap->vend )
        return pcb->_heap;

    /* check stack */
    if(pcb->_heap->vend + PAGE_SIZE <= vaddr && vaddr < pcb->_stack->vend )
    {
        pcb->_stack->vbegin = PAGE_ALIGN(vaddr);
        return pcb->_stack;
    }

    /* check other regions */
    for(as_region * r = pcb->_region; r != NULL ;r = r->next_)
    {
        if(r->vbegin <= vaddr && vaddr <= (r->vend | 0xFFF))
            return r;
    }
    return NULL;
}

void sos_pcb_resize_heap(sos_as *pcb,seL4_Uservaddr vend)
{
    as_region *heap = pcb->_heap;
    if(vend < heap->vend)
    {
        while(vend <= heap->vend - 0x1000)
        {
            if(check_alloc(pcb,heap->vend))
                pagetable_free_page(pcb,heap->vend);
            heap->vend -= 0x1000;
        }
    }
    else if(vend > heap->vend)
        heap->vend = vend;
}

int 
pop_pid()
{
    int offset = proc_manager.cur_offset;
    uint64_t mask;
    do{
        mask = ((uint64_t)1) << offset;
        if(!(proc_manager.bitmap & mask))
        {
            proc_manager.bitmap |= mask;
            proc_manager.cur_offset = (offset + 1) % 64;
            return offset;
        }
        offset = (offset + 1) % 64;
    }while(offset != proc_manager.cur_offset);
    
    return -1;
}

void 
recycle_pid(int pid)
{
    if(pid > 63 || pid < 0) return;
    uint64_t mask =((uint64_t) 1) << pid;
    proc_manager.bitmap &= (~mask);
}

int
check_pid(pid_t pid)
{
    uint64_t mask =((uint64_t) 1) << pid;
    if(mask & proc_manager.bitmap) return 1;

    return 0;
}

void 
process_manager_init()
{
    memset(proc_manager.pcb_arr,0,sizeof(sos_as*) * 64);
    proc_manager.bitmap = 0;
    proc_manager.cur_offset = 0;
    proc_q = _msg_queue_init();
}

int
sos_pcb_process_create(char *pcb_name,seL4_CPtr fault_ep,seL4_CPtr reply_cap)
{
    sync_acquire(lock_arr[PROC_LOCK]);
    int pid;
    pid = pop_pid();
    dprintf(0,"pop new pid = %d,name = %s\n",pid,pcb_name);
    if(pid != -1)
    {
        proc_manager.pcb_arr[pid] = (sos_as *)malloc(sizeof(sos_as));
        if(proc_manager.pcb_arr[pid] == NULL)
        {
            recycle_pid(pid);
            dprintf(0,"Faile to alloc sos_as in sos_pcb_process_create\n");
            pid = -1;
        }
    }


    if(pid != -1)
    {
        memset(proc_manager.pcb_arr[pid],0,sizeof(sos_as));
        proc_manager.pcb_arr[pid]->name = pcb_name;
        proc_manager.pcb_arr[pid]->pid = pid;
        proc_manager.pcb_arr[pid]->reply_cap = reply_cap;
        proc_manager.pcb_arr[pid]->kernel_thread = sos_task_create(wrapper_system_loop,fault_ep,proc_manager.pcb_arr[pid],pid,pid);
        if(proc_manager.pcb_arr[pid]->kernel_thread == NULL)
        {
            dprintf(0,"Faile to create kernel_thread in sos_pcb_process_create\n");
            free(proc_manager.pcb_arr[pid]);
            recycle_pid(pid);
            pid = -1;
        }
    }

    sync_release(lock_arr[PROC_LOCK]);
    return pid;
}

int 
sos_pcb_process_recycle(sos_as *pcb,pid_t pid)
{
    dprintf(0,"do recycle work thread\n");
    sync_acquire(lock_arr[PROC_LOCK]);

    seL4_TCB_Suspend(pcb->kernel_thread ->tcb_cap);
    sos_task_destory(pcb->kernel_thread);
    free(pcb);
    sync_release(lock_arr[PROC_LOCK]);
    dprintf(0,"recycle work thread done\n");
    return 0;
}

int
sos_pcb_process_delete(sos_as *pcb,pid_t pid,seL4_CPtr reply_cap)
{
    dprintf(0,"process : %d request delete process : %d,checkpid= %d\n",pcb->pid,pid,check_pid(pid));
    sync_acquire(lock_arr[PROC_LOCK]);
    /* recycle pid */
    if(!check_pid(pid))
    {
        sos_reply_cap(reply_cap,-1);
        sync_release(lock_arr[PROC_LOCK]);
        return -1;
    }
    sos_as *delete_pcb = proc_manager.pcb_arr[pid];

    /* suspend two threads */
    seL4_TCB_Suspend(delete_pcb->kernel_thread ->tcb_cap);
    seL4_TCB_Suspend(delete_pcb->tcb_cap);

    if(delete_pcb->hold_lock)
    {
        sync_release(lock_arr[PROC_LOCK]);

        dprintf(0,"delete process hold lock resume and wait release\n");
        delete_pcb->async_suspend = 1;
        seL4_TCB_Resume(delete_pcb->kernel_thread ->tcb_cap);
        seL4_TCB_Resume(delete_pcb->tcb_cap);
        return 0;
    }

    dprintf(6,"suspend thread \n");
    dprintf(6,"get delete pcb\n");
    recycle_pid(delete_pcb->pid);

    /* cancel waiting */
    if(delete_pcb->wait_backup)
        delete_pcb->wait_backup->pcb = NULL;
    
    dprintf(6,"remove pcb wait\n");
    /* remove sleep timer */
    sync_acquire(lock_arr[TIME_LOCK]);
    if(delete_pcb->time_id)
        remove_timer(delete_pcb->time_id);
    sync_release(lock_arr[TIME_LOCK]);

    dprintf(6,"remove timer\n");
    /* if kill thread self,do not need reply */
    if(pcb == delete_pcb && reply_cap)
    {
        cspace_free_slot(cur_cspace,reply_cap);
        reply_cap = NULL;
    }

    /* destory application thread */
    destroy_application(delete_pcb);
    dprintf(6,"application thread destory down\n");

    /* destory kernel thread */
    sos_task_destory(delete_pcb->kernel_thread);
    dprintf(6,"work thread destory down\n");

    free(delete_pcb);

    if(reply_cap) 
        sos_reply_cap(reply_cap,0);
    
    sync_release(lock_arr[PROC_LOCK]);
    return 0;
}

int
sos_pcb_process_wait(sos_as *pcb,int pid,seL4_CPtr reply_cap)
{
    if(pid < -1)
    {
        sos_reply_cap(reply_cap,-1);
        return;
    }
    sos_lock_acquire(pcb,PROC_LOCK);
    if(pid > 0)
    {
        if(!check_pid(pid))
        {
            sos_reply_cap(reply_cap,-1);
            sos_lock_release(pcb,PROC_LOCK);
            return -1;
        }
        if(pid == pcb ->pid)
        {
            sos_reply_cap(reply_cap,-1);
            sos_lock_release(pcb,PROC_LOCK);
            return -1;
        }
        if(!(proc_manager.pcb_arr[pid]->isready))
        {
            sos_reply_cap(reply_cap,-1);
            sos_lock_release(pcb,PROC_LOCK);
            return -1;
        }
        wait_list* node = (wait_list *)malloc(sizeof(wait_list));
        memset(node,0,sizeof(wait_list));
        pcb->reply_cap = reply_cap;
        sos_as *des_pcb = proc_manager.pcb_arr[pid];

        node -> pcb = pcb;
        node -> next_ = des_pcb->wait_lst.next_;
        des_pcb->wait_lst.next_ = node;

        pcb->wait_backup = node;
    }
    else
    {
        wait_list* node = (wait_list *)malloc(sizeof(wait_list));
        memset(node,0,sizeof(wait_list));
        pcb->reply_cap = reply_cap;

        node -> pcb = pcb;
        node -> next_ = glb_wait_lst.next_;
        glb_wait_lst.next_ = node;

        pcb->wait_backup = node;
    }
    sos_lock_release(pcb,PROC_LOCK);
}

int
sos_pcb_process_status(sos_as* pcb,seL4_CPtr reply_cap)
{
    int num_p = seL4_GetMR(1);
    sos_lock_acquire(pcb,PROC_LOCK);
    int cur = -1;
    int tmp = -1;

    do
    {
        tmp ++;
        if(check_pid(tmp))
            cur ++;

    }while(tmp < 64 && cur != num_p);

    dprintf(0,"process statue : cur = %d ,tmp = %d num_p = %d\n",cur,tmp,num_p);
    if(cur == num_p)
    {
        sos_as* pcb = proc_manager.pcb_arr[tmp];
        seL4_MessageInfo_t reply = seL4_MessageInfo_new(0, 0, 0, 13);
        seL4_SetMR(0,0);
        seL4_SetMR(1,pcb -> pid);
        seL4_SetMR(2,pcb -> p_size); 
        memcpy((seL4_GetIPCBuffer()->msg + 3),&(pcb->stime),sizeof(uint64_t));
        memcpy((seL4_GetIPCBuffer()->msg + 5),pcb->name,32);//client only receive that much
        seL4_Send(reply_cap, reply);
        cspace_free_slot(cur_cspace, reply_cap);
    }
    else
    {
        sos_reply_cap(reply_cap,-1);
    }

    sos_lock_release(pcb,PROC_LOCK);
    return 0;
}
