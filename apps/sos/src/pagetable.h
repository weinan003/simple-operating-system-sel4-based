#ifndef __PAGE_TABLE_H__
#define __PAGE_TABLE_H__
#include <cspace/cspace.h>
#include "env.h"

#define PAGE_GUARD (0)
#define PAGE_RW (seL4_CanRead|seL4_CanWrite)
#define PAGE_W (seL4_CanWrite)
#define PAGE_R (seL4_CanRead)
#define PAGE_ALL (seL4_AllRights)

//x-> virtual address 
//find out x's pagetable index
#define PAGEL1(x) (x >> 26)
#define PAGEL2(x) ((x >> 20) & 0x3F)
#define PAGEL3(x) ((x >> 12) & 0xFF)

//compress permission,attribute and frameid together
#define PUSH_PERMISSION(x) (x << 29)
#define POP_PERMISSION(info) (info >> 29)

#define PUSH_ATTR(x) ((x - 1) << 26)
#define POP_ATTR(info) (((info >> 26) + 1) & 0x7)

#define POP_PADDR(x) (x & 0x3FFFFFF)
struct sos_as;

#define PAGE_SWAP_FLAG (1<<28)

//first 6 bits use for permission record
//last 26 bits use for phyaddr record
typedef uint32_t* pageinfo;
typedef pageinfo* pageL2;
typedef pageL2* pageL1;

int pagetable_init(struct sos_as* pcb);
void pagetable_free(struct sos_as *pcb);

int pagetable_alloc(struct sos_as* pcb,seL4_CPtr vaddr,seL4_CapRights permission,seL4_ARM_VMAttributes attr);

uint32_t pagetable_vm_fault(struct sos_as* tty_pcb,seL4_CPtr addr);

void pagetable_create_guard(struct sos_as* tty_pcb, seL4_CPtr vaddr);

int pagetable_map_page(struct sos_as* pcb,seL4_Uservaddr vaddr);
int pagetable_unmap_page(struct sos_as* pcb,seL4_Uservaddr vaddr);

int pagetable_switch2soscap(struct sos_as* pcb,seL4_Uservaddr vaddr,seL4_CPtr* cap);
int pagetable_switch2sosaddr(struct sos_as* pcb,seL4_Uservaddr vaddr,seL4_Sosvaddr* kvaddr);
int pagetable_seekpermission(struct sos_as* pcb,seL4_Uservaddr vaddr);
int pagetable_free_page(struct sos_as* pcb,seL4_Uservaddr vaddr);

/* 检查page是否swap out了,检查
 * PAGE_SWAP_FLAG位,swap out了返回TRUE*/
BOOL page_check_swap();
BOOL pageinfo_setSwapFlag(pageinfo);
BOOL pageinfo_unsetSwapFlag(pageinfo pi);

BOOL pageinfo_setSwapIndex(pageinfo, int);
pageinfo get_pageinfo(struct sos_as*pcb, seL4_Uservaddr va);
int pagetable_pin(struct sos_as* pcb,seL4_Uservaddr va);
int pagetable_unpin(struct sos_as* pcb,seL4_Uservaddr va);
int check_alloc(struct sos_as* pcb, seL4_CPtr vaddr);
#endif
