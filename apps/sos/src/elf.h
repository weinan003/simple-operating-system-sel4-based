/*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

#ifndef _LIBOS_ELF_H_
#define _LIBOS_ELF_H_

#include <sel4/sel4.h>
#include "env.h"

struct sos_as;
int elf_load(struct sos_as* pcb, char* elf_file);
int elf_load_region(struct sos_as* pcb,seL4_Sosvaddr buf,size_t offset,size_t length);

#endif /* _LIBOS_ELF_H_ */
