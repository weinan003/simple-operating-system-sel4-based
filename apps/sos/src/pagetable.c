#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <ut_manager/ut.h>

#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>

#include "vmem_layout.h"
#include "pagetable.h"
#include "frametable.h"
#include "pcb.h"
#include "lock.h"

int check_alloc(struct sos_as* pcb, seL4_CPtr vaddr)
{
    uint32_t pL1,pL2,pL3;

    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);
    if(!(pcb->pagetable[pL1]))
        return 0;
    if(!(pcb->pagetable[pL1][pL2]))
        return 0;
    if(pcb->pagetable[pL1][pL2][pL3] == 0)
        return 0;

    return 1;
}

int pagetable_init(struct sos_as* pcb)
{
    pcb->pagetable = (pageL2*)malloc(sizeof(pageL2)*PAGE_LEVEL1_SIZE);
    if(!(pcb->pagetable)) 
        return !0;
    memset(pcb->pagetable,0,sizeof(pageL2) * PAGE_LEVEL1_SIZE);

    pcb->pt_recordlst = (L2_recordnode*)malloc(sizeof(L2_recordnode));
    if(!(pcb->pt_recordlst))
    {
        free(pcb -> pagetable);
        pcb -> pagetable = NULL;
        return !0;
    }
    memset(pcb->pt_recordlst,0,sizeof(L2_recordnode));
    return 0;
}

void pagetable_free(struct sos_as *pcb)
{
    pageL1 pagetable = pcb->pagetable;
    for(int i = 0;i < PAGE_LEVEL1_SIZE;i ++)
    {
        if(pagetable[i])
        {
            for(int j = 0;j < PAGE_LEVEL2_SIZE;j ++)
            {
                if(pagetable[i][j])
                {
                    free(pagetable[i][j]);
                }
            }
            free(pagetable[i]);
            pagetable[i] = NULL;
        }
    }
    free(pagetable);
    pcb -> pagetable = NULL;
}

uint32_t pagetable_vm_fault(struct sos_as* pcb,seL4_Uservaddr addr)
{
    int err;
    addr = addr & 0xFFFFF000;
    err = 0;
    as_region* region = sos_pcb_seek_region(pcb,addr);
    if(!region)
    {
        dprintf(0,"Segmentation Fault : IN USER VADDR :%p\n",addr);
        return 1;
    }

    if(region == pcb->_heap || region == pcb->_stack)
    {
        dprintf(0,"VM FAULT in %p,in heap = %d ,in stack = %d\n",addr,region == pcb->_heap,region == pcb->_stack);

        sos_lock_acquire(pcb,MEM_LOCK);
        if(check_alloc(pcb,addr))
        {
			pageinfo pi = get_pageinfo(pcb, addr);
			if(check_page_status(pi) == SWAPOUT){
				int swaptable_index = get_swaptable_index(pi);
                sos_lock_release(pcb,MEM_LOCK);
				err = swapin(pi, swaptable_index, pcb, addr);
                pagetable_unpin(pcb,addr);
				return err;				
			}
			else{
		    	err = pagetable_map_page(pcb,addr);
                sos_lock_release(pcb,MEM_LOCK);
			}
        }
        else
        {
            sos_lock_release(pcb,MEM_LOCK);
            pagetable_alloc(pcb,addr ,seL4_CanRead|seL4_CanWrite,seL4_ARM_Default_VMAttributes);
            sos_lock_acquire(pcb,MEM_LOCK);
            err = pagetable_map_page(pcb,addr );
            sos_lock_release(pcb,MEM_LOCK);
            pagetable_unpin(pcb,addr);
        }
    }
    else
    {
        sos_lock_acquire(pcb,MEM_LOCK);
        pageinfo pi = get_pageinfo(pcb, addr);
        if(check_page_status(pi) == SWAPOUT){
            int swaptable_index = get_swaptable_index(pi);
            sos_lock_release(pcb,MEM_LOCK);
            err = swapin(pi, swaptable_index, pcb, addr);
            pagetable_unpin(pcb,addr);
            return err;				
        }
        else{
            err = pagetable_map_page(pcb,addr);
            sos_lock_release(pcb,MEM_LOCK);
        }
    }

    return err;
}

int pagetable_free_page(struct sos_as* pcb,seL4_Uservaddr vaddr)
{
    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);
    uint32_t pid = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
    frame_free(pcb,pid);
    pcb->pagetable[pL1][pL2][pL3] = 0;
}

int pagetable_map_page(struct sos_as* pcb,seL4_CPtr vaddr)
{
    seL4_CPtr sos_cap;
    seL4_CPtr user_cap;
    uint32_t pL1,pL2,pL3;
    int err;

    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);
    sos_cap = frame_seek_sos_cap(POP_PADDR(pcb->pagetable[pL1][pL2][pL3]));
    user_cap = cspace_copy_cap(cur_cspace,cur_cspace,sos_cap,POP_PERMISSION(pcb->pagetable[pL1][pL2][pL3]));
    err = seL4_ARM_Page_Map(user_cap, pcb->vroot, vaddr & 0xFFFFF000,
            POP_PERMISSION(pcb->pagetable[pL1][pL2][pL3]),POP_ATTR(pcb->pagetable[pL1][pL2][pL3]));

    if(err != seL4_NoError && err != seL4_FailedLookup)
    {
        dprintf(0,"Failed to Map \n");
        return err;
    }

    if(err == seL4_FailedLookup)
    {
        seL4_CPtr uaddr;
        seL4_ARM_PageTable pt_cap;
        uaddr = ut_alloc(seL4_PageTableBits);
        if(uaddr == 0){
            dprintf(0,"Failed alloc pagetable in pagetable_map_page\n");
            return !0;
        }
        err =  cspace_ut_retype_addr(uaddr, 
                seL4_ARM_PageTableObject,
                seL4_PageTableBits,
                cur_cspace,
                &(pt_cap));
        if(err)
        {
            dprintf(0,"Failed retype pagetable in pagetable_map_page\n");
            ut_free(uaddr,seL4_PageTableBits);
            return err;
        }

        L2_recordnode* node = malloc(sizeof(L2_recordnode));
        if(!node)
        {
            dprintf(0,"Failed malloc L2_recordnode\n");
            cspace_delete_cap(cur_cspace,pt_cap);
            ut_free(uaddr,seL4_PageTableBits);
            return !0;
        }
        memset(node,0,sizeof(L2_recordnode));
        node->pt_cap = pt_cap;
        node->uaddr = uaddr;

        /* Tell seL4 to map the PT in for us */
        err = seL4_ARM_PageTable_Map(pt_cap, 
                pcb->vroot, 
                vaddr & 0xFFFFF000, 
                seL4_ARM_Default_VMAttributes);
        if(err)
        {
            dprintf(0,"Failed map pagetable in pagetable_map_page\n");
            free(node);
            cspace_delete_cap(cur_cspace,pt_cap);
            ut_free(uaddr,seL4_PageTableBits);
            return !0;
        }

        err = seL4_ARM_Page_Map(user_cap, pcb->vroot, vaddr & 0xFFFFF000,
                POP_PERMISSION(pcb->pagetable[pL1][pL2][pL3]),POP_ATTR(pcb->pagetable[pL1][pL2][pL3]));
        if(err)
        {
            dprintf(0, "Failed to map page after creat page table\n");
            return !0;
        }

        node->next_ = pcb->pt_recordlst->next_;
        pcb->pt_recordlst->next_ = node->next_ ;

    }
	uint32_t phy_id = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
	set_frame_ref(phy_id);

    seL4_ARM_Page_Unify_Instruction(user_cap, 0, PAGE_SIZE);
    return err;

}

void pagetable_create_guard(struct sos_as* pcb, seL4_CPtr vaddr)
{
    int err;
    err = pagetable_alloc(pcb, vaddr, PAGE_GUARD,seL4_ARM_Default_VMAttributes );
    conditional_panic(err, "Failed to allocate guard page");
    pagetable_map_page(pcb,vaddr);
}

int pagetable_alloc(struct sos_as* pcb,seL4_CPtr vaddr,seL4_CapRights permission,seL4_ARM_VMAttributes attr)
{
    uint32_t paddr_id;
    uint32_t pL1,pL2,pL3;
    paddr_id = frame_alloc(pcb,vaddr);
    if(!paddr_id)
        paddr_id = swapout(pcb,vaddr);

    if(paddr_id < 1) return 1;

    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);

    sos_lock_acquire(pcb,MEM_LOCK);
    if(!(pcb->pagetable[pL1]))
    {
        pcb->pagetable[pL1] = malloc(sizeof(pageL2) * PAGE_LEVEL2_SIZE);
        if(!(pcb->pagetable[pL1]))
        {
            dprintf(0,"Failed to malloc shadow page dictionary \n");
            frame_free(pcb,paddr_id);
            sos_lock_release(pcb,MEM_LOCK);
            return !0;
        }
        memset((pcb->pagetable[pL1]),0,sizeof(pageL2) * PAGE_LEVEL2_SIZE);
    }

    if(!(pcb->pagetable[pL1][pL2]))
    {
        pcb->pagetable[pL1][pL2] = (pageinfo*) malloc(sizeof(pageinfo) * PAGE_LEVEL3_SIZE);
        if(!(pcb->pagetable[pL1][pL2]))
        {
            dprintf(0,"Failed to malloc shadow pagetable\n");
            frame_free(pcb,paddr_id);
            sos_lock_release(pcb,MEM_LOCK);
            return !0;
        }
        memset((pcb->pagetable[pL1][pL2]),0,sizeof(pageinfo) * PAGE_LEVEL3_SIZE);
    }
    pcb->pagetable[pL1][pL2][pL3]= PUSH_ATTR(attr) | PUSH_PERMISSION(permission) | paddr_id;
    sos_lock_release(pcb,MEM_LOCK);
    return 0;
}
int pagetable_switch2soscap(struct sos_as* pcb,seL4_CPtr vaddr,seL4_CPtr* cap)
{
    if(!check_alloc(pcb,vaddr))
        return 1;

    uint32_t paddr_id;
    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);

    paddr_id = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
    *cap = frame_seek_sos_cap(paddr_id);
    return 0;
}

int pagetable_switch2sosaddr(struct sos_as* pcb,seL4_Uservaddr vaddr,seL4_Sosvaddr* kvaddr)
{
    if(!check_alloc(pcb,vaddr))
        return 1;

    uint32_t paddr_id;
    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);

    paddr_id = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
    *kvaddr = frame_seek_sos_addr(paddr_id);
    *kvaddr |= (vaddr & 0xFFF);
    return 0;
}

int pagetable_seekpermission(struct sos_as* pcb,seL4_CPtr vaddr)
{
    if(!check_alloc(pcb,vaddr))
        return 0;

    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(vaddr);
    pL2 = PAGEL2(vaddr);
    pL3 = PAGEL3(vaddr);

    return POP_PERMISSION(pcb->pagetable[pL1][pL2][pL3]);
}

BOOL pageinfo_setSwapFlag(pageinfo pi){
	*pi  |= PAGE_SWAP_FLAG;
	return TRUE;
}
BOOL pageinfo_unsetSwapFlag(pageinfo pi){
	*pi &= (~PAGE_SWAP_FLAG);
	return TRUE;
}

BOOL pageinfo_setSwapIndex(pageinfo pi, int swapfile_index){
	uint32_t bit = 0xFFF80000;
	*pi  &=  bit;
	*pi |= (swapfile_index | (~bit));
	return TRUE; 
}


pageinfo get_pageinfo(sos_as* pcb,seL4_Uservaddr vaddr)
{
	uint32_t pL1,pL2,pL3;
	pL1 = PAGEL1(vaddr);
	pL2 = PAGEL2(vaddr);
	pL3 = PAGEL3(vaddr);

	return &(pcb->pagetable[pL1][pL2][pL3]);
}

int pagetable_pin(sos_as* pcb,seL4_Uservaddr va)
{
    uint32_t paddr_id;
    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(va);
    pL2 = PAGEL2(va);
    pL3 = PAGEL3(va);

    paddr_id = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
    set_frame_pin(paddr_id);
}

int pagetable_unpin(sos_as* pcb,seL4_Uservaddr va)
{
    uint32_t paddr_id;
    uint32_t pL1,pL2,pL3;
    pL1 = PAGEL1(va);
    pL2 = PAGEL2(va);
    pL3 = PAGEL3(va);

    paddr_id = POP_PADDR(pcb->pagetable[pL1][pL2][pL3]);
    unset_frame_pin(paddr_id);
}
