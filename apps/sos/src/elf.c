/*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

#include <sel4/sel4.h>
#include <elf/elf.h>
#include <string.h>
#include <assert.h>
#include <cspace/cspace.h>

#include "elf.h"
#include "lock.h"

#include <vmem_layout.h>
#include <ut_manager/ut.h>
#include <mapping.h>

#define verbose 2
#include <sys/debug.h>
#include <sys/panic.h>

#include "pcb.h"
#include "frametable.h"
#include "pagetable.h"
#include "file.h"
#include "file_callback.h"
/* Minimum of two values. */
#define MIN(a,b) (((a)<(b))?(a):(b))

#define PAGESIZE              (1 << (seL4_PageBits))
#define PAGEMASK              ((PAGESIZE) - 1)
#define PAGE_ALIGN(addr)      ((addr) & ~(PAGEMASK))
#define IS_PAGESIZE_ALIGNED(addr) !((addr) &  (PAGEMASK))


extern seL4_ARM_PageDirectory dest_as;
extern union u_frame* frametable;
extern struct page_level1_node* pagetable__;

static read_token region_token[4];
int elf_load_region(sos_as *pcb,seL4_Sosvaddr buf,size_t offset,size_t length)
{

    for(int i = 0;i < 4;i ++)
    {
        if(length)
        {
            region_token[i].isready = 0;
            region_token[i].data = (char *)(buf + TOKEN_BUF_SIZ * i);
            region_token[i].err = nfs_read(
                    &(pcb->proc_inode->fh),
                    offset + TOKEN_BUF_SIZ * i,
                    MIN(TOKEN_BUF_SIZ,length),
                    &_read_cb,
                    (uintptr_t)(region_token + i));
            length -=  MIN(TOKEN_BUF_SIZ,length);
        }
        else
        {
            region_token[i].isready = 1;
            region_token[i].status = 0;
            region_token[i].err = 0;
        }
    }

    if(region_token[0].err || region_token[1].err 
            || region_token[2].err || region_token[3].err)
        return VFS_ERROR;

    while(!(region_token[0].isready && region_token[1].isready 
                && region_token[2].isready && region_token[3].isready))
        seL4_Yield();

    /* check nfs status */
    if(region_token[0].status || region_token[1].status 
            || region_token[2].status || region_token[3].status)
        return VFS_ERROR;

	return VFS_OK;
}

/*
 * Convert ELF permissions into seL4 permissions.
 */
static inline 
seL4_Word 
get_sel4_rights_from_elf(unsigned long permissions) {
    seL4_Word result = 0;

    if (permissions & PF_R)
        result |= seL4_CanRead;
    if (permissions & PF_X)
        result |= seL4_CanRead;
    if (permissions & PF_W)
        result |= seL4_CanWrite;

    return result;
}

/*
 * Inject data into the given vspace.
 * TODO: Don't keep these pages mapped in
 */
static int 
load_segment_into_vspace(struct sos_as* pcb,
                                    size_t offset , unsigned long segment_size,
                                    unsigned long file_size, unsigned long dst,
                                    unsigned long permissions) {

    int err ;
    unsigned long pos;
    assert(file_size <= segment_size);

    /* We work a page at a time in the destination vspace. */
    as_region *region = (as_region *) malloc(sizeof(as_region));
    if(! region) return 1;

    memset(region,0,sizeof(as_region));
    region->vbegin = PAGE_ALIGN(dst);
    region->vend = PAGE_ALIGN(region->vbegin + segment_size) + PAGESIZE;

    pos = 0;
    while(pos < segment_size) {
        seL4_CPtr sos_cap;
        seL4_Uservaddr vpage;
        seL4_Sosvaddr kdst;
        int nbytes;
        vpage  = PAGE_ALIGN(dst);

        /* First we need to create a frame */
        err = pagetable_alloc(pcb,vpage,permissions,seL4_ARM_Default_VMAttributes);
        if(err)
        {
            vpage -= PAGESIZE;
            for(;vpage >= region -> vbegin;vpage -= PAGESIZE)
                pagetable_free_page(pcb,vpage);
            free(region);
            return err;
        }

        sos_lock_acquire(pcb,MEM_LOCK);
        err = pagetable_map_page(pcb,vpage);
        sos_lock_release(pcb,MEM_LOCK);
        if( err )
        {
            dprintf(0,"Failed to map page in elf\n");
            for(;vpage >= region -> vbegin;vpage -= PAGESIZE)
                pagetable_free_page(pcb,vpage);

            free(region);
            return err;
        }

        pagetable_switch2sosaddr(pcb,dst,&kdst);
        pagetable_switch2soscap(pcb,dst,&sos_cap);

        nbytes = PAGESIZE - (dst & PAGEMASK);
        if (pos < file_size)
        {
            err = elf_load_region(pcb,kdst,offset,MIN(nbytes,file_size - pos));
            if( err )
            {
                dprintf(0,"ELF load err\n");
                for(;vpage >= region -> vbegin;vpage -= PAGESIZE)
                    pagetable_free_page(pcb,vpage);
                free(region);
                return err;
            }
        }

        // Not observable to I-cache yet so flush the frame 
        seL4_ARM_Page_Unify_Instruction(sos_cap, 0, PAGESIZE);
        pagetable_unpin(pcb,vpage);

        pos += nbytes;
        dst += nbytes;
        offset += nbytes;
    }

    sos_pcb_add_region(pcb,region);

    return 0;
}

int elf_load(struct sos_as* pcb, char *elf_file) {

    int num_headers;
    int err;
    int i;

    /* Ensure that the ELF file looks sane. */
    if (elf_checkFile(elf_file)){
        return seL4_InvalidArgument;
    }

    num_headers = elf_getNumProgramHeaders(elf_file);
    for (i = 0; i < num_headers; i++) {
        size_t head_off;
        unsigned long flags, file_size, segment_size, vaddr;

        /* Skip non-loadable segments (such as debugging data). */
        if (elf_getProgramHeaderType(elf_file, i) != PT_LOAD)
            continue;

        /* Fetch information about this segment. */
        head_off = elf_getProgramHeaderOffset(elf_file, i);
        file_size = elf_getProgramHeaderFileSize(elf_file, i);
        segment_size = elf_getProgramHeaderMemorySize(elf_file, i);
        vaddr = elf_getProgramHeaderVaddr(elf_file, i);
        flags = elf_getProgramHeaderFlags(elf_file, i);

        /* Copy it across into the vspace. */
        dprintf(0, " * Loading segment %08x-->%08x\n", (int)vaddr, (int)(vaddr + segment_size));
        err = load_segment_into_vspace(pcb, head_off, segment_size, file_size, vaddr,
                                       get_sel4_rights_from_elf(flags) & seL4_AllRights);

        if( err ) return seL4_RangeError;
    }

    return 0;
}
