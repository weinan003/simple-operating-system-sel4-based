#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <cspace/cspace.h>
#include <serial/serial.h>
#include "vfs.h"
#include "env.h"
#include "frametable.h"
#include "swaptable.h"
#include "pcb.h"
#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include "vmem_layout.h"
#include "console.h"
#include "task.h"
#include "lock.h"

#define MAX_PAYLOAD_SIZE  500
#define MIN(a,b) (((a)<(b))?(a):(b))
#define CONSOLE_READ (0)
#define CONSOLE_WRITE (1)

#define CONSOLE_SUBTHREAD_BADGE (1000)

static struct serial* srl;

static int con_close(sos_vfs_vnode* vnode);
static int con_open(seL4_File_Descripter local_id,seL4_File_Descripter *glb_fd,sos_vfs_vnode **p_vnode,seL4_Buffer path,seL4_CPtr reply_cap);
static int con_read(sos_as *pcb,nfs_inode *inode_id,uint32_t *offset,seL4_Uservaddr buf,size_t length,seL4_CPtr reply_cap);
static int con_write(sos_as *pcb,nfs_inode *inode_id,uint32_t *offset,seL4_Uservaddr ubuf,size_t length,seL4_CPtr reply_cap);

static char buffer[8192];
static uint32_t readBufferOff,glb_readBuffer;

void 
serial_handler(struct serial *serial, char c)
{
    char *ptr = (char *)readBufferOff;
    *ptr = c;
    readBufferOff += 1;
}

int 
sos_console_init(sos_vfs_operation* op,seL4_CPtr sos_ep)
{
    /* sanity check */
    if(!op)
        return VFS_ERROR;
    if(srl)
        return VFS_ERROR;


    srl = serial_init();
    conditional_panic((!srl),"Failed to init serial device");

    op->vop_open = &con_open;
    op->vop_read = &con_read;
    op->vop_write= &con_write;
    op->vop_close = &con_close;

    glb_readBuffer = buffer;

    return VFS_OK;
}

static int open4read = 0;
static int 
con_close(sos_vfs_vnode* vnode)
{
    if( vnode-> permission & FM_READ )
        open4read = 0;
}

static int 
con_open(seL4_File_Descripter local_id,
        seL4_File_Descripter *glb_fd,
        sos_vfs_vnode **p_vnode,
        seL4_Buffer path,
        seL4_CPtr reply_cap)
{

    if((*p_vnode)-> permission & FM_READ )
    {
        if(open4read == 0)
        {
            dprintf(0,"console :accept one read request\n");
            open4read += 1;
            if (reply_cap)
                sos_reply_cap(reply_cap,local_id);
        }
        else
        {
            dprintf(0,"console :decline one read request\n");
            free(*p_vnode);
            *p_vnode = NULL;
            *glb_fd = -1;

            if (reply_cap)
                sos_reply_cap(reply_cap,-1);
        }
    }
    else
    {
        if (reply_cap)
            sos_reply_cap(reply_cap,local_id);
    }

    return VFS_OK;
}

static int 
con_read(sos_as *pcb,
        nfs_inode *inode,
        uint32_t *offset,
        seL4_Uservaddr buf,
        size_t length,
        seL4_CPtr reply_cap)
{

    length = MIN(length,PAGE_SIZE * 2);

    serial_register_handler(srl , &serial_handler);
    readBufferOff = glb_readBuffer;
    bzero(glb_readBuffer,PAGE_SIZE * 2);
    while((readBufferOff - glb_readBuffer) < length)
    {
        if((readBufferOff != glb_readBuffer) &&
                ((*(char*)(readBufferOff - 1) == '\n') ||
                 (*(char*)(readBufferOff - 1) == '\r')))
            break;

        seL4_Yield();

    }
    serial_register_handler(srl , NULL);

    int err;
    size_t seglen;
    seL4_Sosvaddr map_buf;
    seglen = MIN(length,PAGE_SIZE - ((uint32_t)(buf) & PAGE_OFFSET_FRAME));

    if(check_alloc(pcb,buf))
    {
        sos_lock_acquire(pcb,MEM_LOCK);
        pageinfo pi = get_pageinfo(pcb, buf);
        if(check_page_status(pi) == SWAPOUT)
        {
            sos_lock_release(pcb,MEM_LOCK);

            int swaptable_index = get_swaptable_index(pi);
            err = swapin(pi, swaptable_index, pcb, buf);
            if(err)
            {
                dprintf(0,"Failed in con_read : process = %d, due to swapin err\n",pcb -> pid);
                sos_reply_cap(reply_cap,-1);
                return VFS_ERROR;
            }
        }
        else
        {
            pagetable_pin(pcb,buf);
            sos_lock_release(pcb,MEM_LOCK);
        }
    }
    else
    {
        err = pagetable_alloc(pcb,buf & PAGE_FRAME,seL4_CanRead|seL4_CanWrite,seL4_ARM_Default_VMAttributes);
        if(err)
        {
                dprintf(0,"Failed in con_read :process = %d, due to pagetable alloc err\n",pcb -> pid);
                sos_reply_cap(reply_cap,-1);
                return VFS_ERROR;
        }
    }

    pagetable_switch2sosaddr(pcb,buf,&map_buf);
    strncpy((void *)map_buf,(void *)glb_readBuffer ,MIN(seglen,(readBufferOff - glb_readBuffer)));
    pagetable_unpin(pcb,buf);

    sos_reply_cap(reply_cap,seglen);
    return VFS_OK;
}

static int 
con_write(sos_as *pcb,
        nfs_inode *inode,
        uint32_t *offset,
        seL4_Uservaddr ubuf,
        size_t length,
        seL4_CPtr reply_cap)
{
    int err;
    size_t seglen,send_count,backup_len;
    seL4_Sosvaddr map_buf;
    send_count = 0;
    while(length)
    {
        seglen = MIN(length,PAGE_SIZE - (ubuf & PAGE_OFFSET_FRAME));
        backup_len = seglen;

        if(check_alloc(pcb,ubuf))
        {
            sos_lock_acquire(pcb,MEM_LOCK);
            pageinfo pi = get_pageinfo(pcb, ubuf);
            if(check_page_status(pi) == SWAPOUT)
            {
                sos_lock_release(pcb,MEM_LOCK);
                int swaptable_index = get_swaptable_index(pi);
                err = swapin(pi, swaptable_index, pcb, ubuf);
                if(err)
                {
                    dprintf(0,"Failed in con_write :process = %d, due to swapin err\n",pcb -> pid);
                    sos_reply_cap(reply_cap,-1);
                    return VFS_ERROR;
                }
            }
            else
            {
                pagetable_pin(pcb,ubuf);
                sos_lock_release(pcb,MEM_LOCK);
            }
        }
        else
        {
            err = pagetable_alloc(pcb,ubuf & PAGE_FRAME,seL4_CanRead|seL4_CanWrite,seL4_ARM_Default_VMAttributes);
            if(err)
            {
                dprintf(0,"Failed in con_write:process = %d, due to pagetable alloc err\n",pcb -> pid);
                sos_reply_cap(reply_cap,-1);
                return VFS_ERROR;
            }
        }

        pagetable_switch2sosaddr(pcb,ubuf,&map_buf);

        while(seglen)
        {
            send_count +=serial_send(srl,(char*)(map_buf),MIN(seglen,MAX_PAYLOAD_SIZE));
            seglen -= MIN(seglen,MAX_PAYLOAD_SIZE);
            map_buf += MIN(seglen,MAX_PAYLOAD_SIZE);
        }

        length -= backup_len;
        ubuf += backup_len;
        pagetable_unpin(pcb,ubuf);
    }

    if(reply_cap)
        sos_reply_cap(reply_cap,send_count);

    return 0;
}
