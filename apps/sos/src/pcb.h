#ifndef __PCB__
#define __PCB__
#include <cspace/cspace.h>
#include <sys/types.h>
#include "env.h"
#include "task.h"
#include "pagetable.h"

#define AS_FD_MAX 128
typedef struct task_node task_node;
typedef struct L2_recordnode L2_recordnode;
typedef struct sos_as sos_as;
typedef struct as_region as_region;
typedef struct nfs_inode nfs_inode;

typedef struct
{
    uint64_t bitmap;
    int cur_offset;
    sos_as* pcb_arr[64];
} process_manager;

msg_queue *proc_q;
process_manager proc_manager;
int pop_pid();
void recycle_pid(pid_t pid);
void process_manager_init();
int check_pid(pid_t pid);


struct L2_recordnode
{
    seL4_ARM_PageTable pt_cap;
    seL4_CPtr uaddr;
    struct L2_recordnode* next_;
};


struct as_region
{
    seL4_Uservaddr vbegin;
    seL4_Uservaddr vend;

    struct as_region *next_;
};

typedef struct wait_list
{
    sos_as* pcb;
    struct wait_list *next_;
}wait_list;

wait_list glb_wait_lst;

struct sos_as{
    pid_t pid;

    seL4_Word tcb_addr;
    seL4_TCB tcb_cap;

    seL4_Word vroot_addr;
    seL4_ARM_PageDirectory vroot;
    L2_recordnode* pt_recordlst;

    cspace_t *croot;
    pageL1 pagetable;

    seL4_File_Descripter fd_table[AS_FD_MAX];

    as_region *_region;
    as_region *_heap,*_stack;

    char* name;
    task_node *kernel_thread;
    seL4_CPtr ipc_ep;
    seL4_Word ipc_ut_addr;

    wait_list wait_lst;
    wait_list* wait_backup;

    seL4_CPtr reply_cap;
    int time_id;

    uint64_t stime;
    uint32_t p_size;

    int hold_lock;
    int async_suspend;

    nfs_inode* proc_inode;
    int isready;
} ;

int sos_pcb_init_space(sos_as* pcb);
int sos_pcb_init_tcb(sos_as* pcb,seL4_CPtr fault_ep);

void sos_pcb_add_region(sos_as *pcb,as_region *region);
as_region* sos_pcb_seek_region(sos_as *pcb,seL4_Uservaddr vaddr);
void sos_pcb_resize_heap(sos_as *pcb,seL4_Uservaddr vend); 

int sos_pcb_process_create(char *pcb_name,seL4_CPtr fault_ep,seL4_CPtr reply_cap);
int sos_pcb_process_delete(sos_as *pcb,pid_t pid,seL4_CPtr reply_cap);
int sos_pcb_process_wait(sos_as *pcb,int pid,seL4_CPtr reply_cap);
int sos_pcb_process_status(sos_as *pcb,seL4_CPtr reply_cap);

int create_application(sos_as *pcb);
void destroy_application(sos_as *pcb);
#endif
