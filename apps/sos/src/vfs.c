#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include "pagetable.h"
#include "vfs.h"
#include "file.h"
#include "file_callback.h"
#include "lock.h"

static sos_vfs_vnode *glb_ftable[FILE_TABLE_SIZE];
static sos_vfs_operation *_op_table[MAX_DEVICE_NUMBER];

int 
sos_vfs_init()
{
    memset(glb_ftable,0,sizeof(sos_vfs_vnode *) * FILE_TABLE_SIZE);
    memset(_op_table,0,sizeof(sos_vfs_operation *) * MAX_DEVICE_NUMBER);
    return VFS_OK;

}

sos_vfs_operation * 
sos_vfs_create_op()
{
    sos_vfs_operation *op = (sos_vfs_operation *)malloc(sizeof(sos_vfs_operation));
    memset(op,0,sizeof(sos_vfs_operation));
    return op;
}

int 
sos_vfs_set_op(sos_vfs_operation* op,uint32_t id)
{
    if(!op) return VFS_ERROR;
    if(id >= MAX_DEVICE_NUMBER) return VFS_ERROR;
    if(_op_table[id]) return VFS_ERROR;

    _op_table[id] = (sos_vfs_operation *)malloc(sizeof(sos_vfs_operation));
    if(!_op_table[id]) return VFS_ERROR;

    _op_table[id] = op;
    return VFS_OK;
}

void 
sos_vfs_close(sos_as *pcb,seL4_File_Descripter local_id,seL4_CPtr reply_cap)
{
    if(local_id < 0 || local_id > AS_FD_MAX - 1)
    {
        dprintf(0,"File closed error ,local fd = %d\n",local_id);
        if(reply_cap) sos_reply_cap(reply_cap,-1);
        return;
    }

    if(pcb->fd_table[local_id] == -1)
    {
        if(reply_cap) sos_reply_cap(reply_cap,0);
        return;
    }

    sos_lock_acquire(pcb,VFS_LOCK);
    sos_vfs_vnode* vnode = sos_vfs_seek_vnode(pcb->fd_table[local_id]);
    vnode->op->vop_close(vnode);
    sos_lock_release(pcb,VFS_LOCK);

    sos_vfs_delete_vnode(pcb,pcb->fd_table[local_id]);
    pcb->fd_table[local_id] = -1;

    if(reply_cap) sos_reply_cap(reply_cap,0);
}
void sos_vfs_open(sos_as *pcb,seL4_File_Descripter local_id,seL4_Buffer path,fmode_t mode,seL4_CPtr reply_cap)
{
    uint32_t id;
    sos_vfs_vnode* vnode;
    sos_vfs_operation *op;
    if(!strncmp(path,"swapfile",8))
    {
        dprintf(0,"swapfile is used for memory swap,cannot use for application RW\n");
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
        return;
    }

    id = strncmp(path,"console",7) ? 1:0;
    sos_vfs_seek_operation(id,&op);
    if(!op)
    {
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
        return;
    }

    pcb->fd_table[local_id] = sos_vfs_create_vnode(pcb);
    vnode = sos_vfs_seek_vnode(pcb->fd_table[local_id]);
    if(pcb->fd_table[local_id] == -1)
    {
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
        return;
    }

    vnode->permission = mode;
    vnode->pid = pcb->pid;
    vnode->op = op;

    sos_lock_acquire(pcb,VFS_LOCK);
    op->vop_open(local_id,&(pcb->fd_table[local_id]),&(glb_ftable[pcb->fd_table[local_id]]),path,reply_cap);
    sos_lock_release(pcb,VFS_LOCK);
}

void 
sos_vfs_seek_operation(int id,sos_vfs_operation **op)
{
    *op = NULL;
    if (id < 0 || id > MAX_DEVICE_NUMBER - 1) return;
    *op = _op_table[id];
}

void 
sos_vfs_delete_vnode(sos_as* pcb,seL4_File_Descripter glb_id)
{
    if(glb_id < 0 || glb_id > FILE_TABLE_SIZE - 1) return ;
    if(!glb_ftable[glb_id]) return ;

    sos_lock_acquire(pcb,VFS_LOCK);
    if(glb_ftable[glb_id] != NULL)
    {
        free(glb_ftable[glb_id]->inode);
        free(glb_ftable[glb_id]);
        glb_ftable[glb_id] = NULL;
    }
    sos_lock_release(pcb,VFS_LOCK);
}

sos_vfs_vnode* 
sos_vfs_seek_vnode(seL4_File_Descripter glb_id)
{
    return glb_ftable[glb_id];
}

seL4_File_Descripter 
sos_vfs_create_vnode(sos_as* pcb)
{
    sos_vfs_vnode* vnode;
    int i ;
    sos_lock_acquire(pcb,VFS_LOCK);
    for(i = 0;i < FILE_TABLE_SIZE;i++)
    {
        if(glb_ftable[i] == NULL)
        {
            vnode = (sos_vfs_vnode *)malloc(sizeof(sos_vfs_vnode));
            if(!vnode) 
            {
                sos_lock_release(pcb,VFS_LOCK);
                return -1;
            }
            memset(vnode,0,sizeof(sos_vfs_vnode));
            glb_ftable[i] = vnode;
            break;
        }
    }
    sos_lock_release(pcb,VFS_LOCK);
    if(i == FILE_TABLE_SIZE)
        return -1;

    return i;
}

void 
sos_vfs_read(sos_as *pcb,seL4_File_Descripter glb_index,seL4_Uservaddr cbuf,size_t lenght,seL4_CPtr reply_cap)
{
    sos_vfs_vnode* vnode = glb_ftable[glb_index];
    if(cbuf == NULL)
    {
        dprintf(0,"buffer is NULL return error\n");
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
        return;
    }

    if(vnode->permission & FM_READ)
    {
        sos_vfs_operation* op = vnode->op;
        op->vop_read(pcb,vnode->inode,&(vnode->offset),cbuf,lenght,reply_cap);
    }
    else
    {
        dprintf(0,"Failed to read,mode does not match\n");
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
    }
}

void 
sos_vfs_write(sos_as *pcb,seL4_File_Descripter glb_index,seL4_Uservaddr ubuf,size_t length,seL4_CPtr reply_cap)
{
    sos_vfs_vnode* vnode = glb_ftable[glb_index];
    if(ubuf == NULL)
    {
        dprintf(0,"buffer is NULL return error\n");
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
        return;
    }

    if(vnode->permission & FM_WRITE)
    {
        sos_vfs_operation* op = vnode->op;
        op->vop_write(pcb,vnode->inode, &(vnode->offset),ubuf,length,reply_cap);
    }
    else
    {
        if(reply_cap)
            sos_reply_cap(reply_cap,-1);
    }
}

void 
sos_vfs_getdirent(int pos,seL4_CPtr reply_cap)
{
    sos_vfs_operation *op;
    sos_vfs_seek_operation(1,&op);
    op->vop_getdirent(pos,reply_cap);
}

void 
sos_vfs_stat(seL4_Buffer path,seL4_CPtr reply_cap)
{
    sos_vfs_operation *op;
    sos_vfs_seek_operation(1,&op);
    op->vop_stat(path,reply_cap);
}
