#include "frametable.h"
#include "vmem_layout.h"

#include <ut_manager/ut.h>
#include <cspace/cspace.h>

#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>
#include "mapping.h"
#include "lock.h"

uint32_t v_mem_off;
static union u_frame* frametable = (void *)0;

void recycle_node(uint32_t id);
union u_frame* pop_free_node();

void frame_init()
{
    seL4_Word low,high;
    seL4_CPtr cap;
    int err;
    uint32_t size,page,addr;

    ut_find_memory(&low,&high);
    //head node use for point next free table
    FRAMETABLE_SIZE  = (high - low) >> seL4_PageBits;

    if(FRAMETABLE_SIZE > MAX_PHY_PAGE_SIZE) FRAMETABLE_SIZE = MAX_PHY_PAGE_SIZE;
    dprintf(0,"frame table hold = %d page slot\n",FRAMETABLE_SIZE);

    size = sizeof(union u_frame) * FRAMETABLE_SIZE;
    page = (size >> seL4_PageBits) + 1;


    for(int i = 0;i < page ;i++)
    {
        addr = ut_alloc(seL4_PageBits);
        err = cspace_ut_retype_addr(addr, 
                seL4_ARM_SmallPageObject,
                seL4_PageBits,
                cur_cspace,
                &cap);
        conditional_panic(err,"Failed to retype frame table addr");

        err = map_page(cap,seL4_CapInitThreadPD,
                PROCESS_HEAP_START + (v_mem_off<<seL4_PageBits),
                seL4_AllRights,seL4_ARM_PageCacheable);

        conditional_panic(err,"map_page Failed");
        v_mem_off++;
    }

    frametable = (union u_frame*) PROCESS_HEAP_START;
    frametable[0].head.nextfree_ = &frametable[1];
}

uint32_t frame_alloc(sos_as *pcb, seL4_Uservaddr va)
{
    uint32_t id;
    union u_frame* node;
    int err;
    conditional_panic(!frametable,"Failed to alloc frame table due to uninitialized");

    /* id is the offset of frametable,so if id larger than FRAMETABLE_SIZE , 
     * that's node is illegal .recycle it and swapout a page do pop again */

    sos_lock_acquire(pcb,MEM_LOCK);
    node = pop_free_node();
    id = node - frametable;
	if(id > FRAMETABLE_SIZE){
        dprintf(0,"frame table id larger than boundary\n");
        recycle_node(id);
        sos_lock_release(pcb,MEM_LOCK);
        return 0;
	}
    set_frame_pin(id);

    node->n.addr = ut_alloc(seL4_PageBits);

    /* return if no more pyhsical memory */
    if(!node->n.addr)
    {
        dprintf(0,"no ut memory can use\n");
        recycle_node(id);
        sos_lock_release(pcb,MEM_LOCK);
        return 0;
    }

    err = cspace_ut_retype_addr(node->n.addr, 
            seL4_ARM_SmallPageObject,
            seL4_PageBits,          
            cur_cspace,             
            &(node->n.cap));        
    if(err)
    {
        dprintf(0,"Failed t o retype frame table addr\n");
        ut_free(node -> n.addr,seL4_PageBits);
        recycle_node(id);
        sos_lock_release(pcb,MEM_LOCK);
        return 0;
    }

    err = map_page(node->n.cap,seL4_CapInitThreadPD,
            PROCESS_HEAP_START + ((id + v_mem_off)<<seL4_PageBits),
            seL4_AllRights,seL4_ARM_PageCacheable);
    if(err)
    {
        dprintf(0,"map_page in sos Failed\n");
        cspace_delete_cap(cur_cspace,node -> n.cap);
        ut_free(node -> n.addr,seL4_PageBits);
        recycle_node(id);
        sos_lock_release(pcb,MEM_LOCK);
        return 0;
    }

    bzero(PROCESS_HEAP_START + ((id + v_mem_off)<<seL4_PageBits),4096);
	node->n.pcb = pcb;
	node->n.va = va;

    sos_lock_release(pcb,MEM_LOCK);

    return id;
}

void frame_free(sos_as* pcb,uint32_t id)
{
    int err;
    sos_lock_acquire(pcb,MEM_LOCK);

    if(frame_check_ref(id))
        err = cspace_revoke_cap(cur_cspace, frametable[id].n.cap);

    frametable[id].n.pcb = NULL;
    frametable[id].n.va = 0;

    err = seL4_ARM_Page_Unmap(frametable[id].n.cap);
    conditional_panic(err,"Failed to Unmap frame memory in SOS virtual address space");

    cspace_delete_cap(cur_cspace,frametable[id].n.cap);
    ut_free(frametable[id].n.addr,seL4_PageBits);

    recycle_node(id);
    sos_lock_release(pcb,MEM_LOCK);
}

void recycle_node(uint32_t id)
{
    frametable[id].head.nextfree_ = frametable[0].head.nextfree_;
    frametable[0].head.nextfree_ = &(frametable[id]);
}

union u_frame* pop_free_node()
{
    union u_frame* pRet;
    if(! frametable)
        return NULL;

    pRet = frametable[0].head.nextfree_;

    //no more physic table
    if(!pRet)
        return pRet;

    frametable[0].head.nextfree_ = (pRet->head.nextfree_ == NULL) ? (pRet + 1):pRet->head.nextfree_;

    return pRet;
}

uint32_t frame_seek_sos_cap(uint32_t phy_id)
{
    return frametable[phy_id].n.cap;
}

seL4_Sosvaddr frame_seek_sos_addr(uint32_t phy_id)
{
    return PROCESS_HEAP_START + ((phy_id + v_mem_off)<<seL4_PageBits);
}

uint32_t frame_seek_phy_id(uint32_t kvaddr)
{
    return ((kvaddr - PROCESS_HEAP_START) >> seL4_PageBits) - v_mem_off;
}

BOOL frame_check_ref(int index){
	if(frametable[index].n.ref & BITREF)
		return TRUE;
	else
		return FALSE;
}
BOOL frame_check_pin(int index){
	return (frametable[index].n.ref & BITPIN);
}

BOOL frame_check_alloc(int index){
	return (frametable[index].n.pcb);
}

void set_frame_ref(int index){
	frametable[index].n.ref |= BITREF;
}
void unset_frame_ref(int index) {
	frametable[index].n.ref &= (~BITREF);
}

int pin_num = 0;
void set_frame_pin(int index) {
    pin_num ++;
	frametable[index].n.ref |= BITPIN;
}

void unset_frame_pin(int index) {
    pin_num --;
	frametable[index].n.ref &= (~BITPIN);
}

sos_as * frame_seek_pcb(int index)
{
    return frametable[index].n.pcb;
}

seL4_Uservaddr frame_seek_Userpage(int index)
{
    return frametable[index].n.va;
}

void frame_set_pcb(int index,sos_as *pcb,seL4_Uservaddr vaddr)
{
    frametable[index].n.pcb = pcb;
    frametable[index].n.va = vaddr;
}
