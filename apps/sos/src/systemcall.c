#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <cspace/cspace.h>

#include <clock/clock.h>
#include <nfs/nfs.h>
#include <elf/elf.h>
#include <serial/serial.h>

#include "network.h"
#include "elf.h"

#include "ut_manager/ut.h"
#include "vmem_layout.h"
#include "mapping.h"
#include "pagetable.h"
#include "frametable.h"
#include "console.h"
#include "systemcall.h"
#include "pcb.h"
#include "vfs.h"
#include "task.h"
#include "lock.h"
#include <autoconf.h>

#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>

#include "file.h"
#include "env.h"

#define PAGESIZE              (1 << (seL4_PageBits))
#define PAGEMASK              ((PAGESIZE) - 1)
#define PAGE_ALIGN(addr)      ((addr) & ~(PAGEMASK))
#define IS_PAGESIZE_ALIGNED(addr) !((addr) &  (PAGEMASK))

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
extern seL4_CPtr _sos_ipc_ep_cap;


static inline int 
transform_file_permission(int flags)
{
    if(flags > O_ACCMODE)
        return -1;
    switch(flags)
    {
        case O_ACCMODE:
            return FM_WRITE | FM_READ | FM_EXEC;
        case O_RDONLY:
            return FM_READ;
        case O_WRONLY:
            return FM_WRITE;
        case O_RDWR:
            return FM_READ | FM_WRITE;
    }
}
static char *
ustr2sosstr(sos_as *pcb,seL4_Uservaddr path_uaddr,size_t length)
{
    as_region* region = sos_pcb_seek_region(pcb,path_uaddr);
    if(region == NULL 
            && region != pcb -> _heap 
            && region != pcb -> _stack)
    {
        dprintf(0,"Failed to translate to sos string,due to segment error\n");
        return NULL;
    }

    char* sos_path = malloc(length + 1);
    if(!sos_path)
        return NULL;
    seL4_Sosvaddr map_buf;
    bzero(sos_path,length + 1);
    size_t seglen;
    char* ptr = sos_path;
    while(length)
    {
        seglen = MIN(length,PAGE_SIZE - (path_uaddr & PAGE_OFFSET_FRAME));
        sos_lock_acquire(pcb,MEM_LOCK);

        pageinfo pi = get_pageinfo(pcb, path_uaddr);
        if(check_page_status(pi) == SWAPOUT)
        {
            sos_lock_release(pcb,MEM_LOCK);
            int swaptable_index = get_swaptable_index(pi);
            if(swapin(pi, swaptable_index, pcb, path_uaddr))
            {
                dprintf(0,"Failed swapin user string\n",pcb -> pid);
                return NULL;
            }

        }
        else
        {
            pagetable_pin(pcb,path_uaddr);
            sos_lock_release(pcb,MEM_LOCK);
        }

        pagetable_switch2sosaddr(pcb,path_uaddr,&map_buf);
        memcpy(ptr,map_buf,seglen);
        pagetable_unpin(pcb,path_uaddr);
        ptr += seglen;
        path_uaddr += seglen;
        length -= seglen;
    }
    return sos_path;
}

void 
systemcall_proc_create(sos_as* pcb,
        seL4_CPtr reply_cap)
{
    int ret;
    size_t length;
    seL4_Uservaddr path_uaddr;
    length = seL4_GetMR(1);
    path_uaddr = seL4_GetMR(2);

    if(length == 0)
    {
        dprintf(0,"Failed to open due to path is null\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    if(path_uaddr == NULL)
    {
        dprintf(0,"Segment fault in open\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    char *path = ustr2sosstr(pcb,path_uaddr,length);

    if(!path)
    {
        dprintf(0,"Failed to malloc in system call proc creat\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    if(strlen(path) > MAXNAMLEN)
    {
        dprintf(0,"Failed to create process,name to long \n"); 
        free(path);
        sos_reply_cap(reply_cap,-1);
        return ;
    }

    struct msg* pMsg =(struct msg *)malloc(sizeof(struct msg));
    if(!pMsg)
    {
        dprintf(0,"Failed to malloc in system call proc creat2\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }
    pMsg ->para_0 = SOS_SYSCALL_PROC_CREATE;
    pMsg ->para_1 = path;
    pMsg ->reply_cap = reply_cap;

    _msg_queue_push_back(proc_q,pMsg);
}

void 
systemcall_proc_delete(sos_as* pcb,
        seL4_CPtr reply_cap)
{
    int pid;
    pid = seL4_GetMR(1);

    if(pid < -1 )
    {
        sos_reply_cap(reply_cap,-1);
        return;
    }
    /* pid == -1 means kill this process exit,do not need reply*/
    if(pid == -1) 
        pid = pcb->pid;

    struct msg* pMsg =(struct msg *)malloc(sizeof(struct msg));
    if(!pMsg)
    {
        dprintf(0,"Failed to malloc in system call proc delete\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    pMsg ->para_0 = SOS_SYSCALL_PROC_DELETE;
    pMsg ->para_1 = pcb;
    pMsg ->para_2 = pid;
    pMsg ->reply_cap = reply_cap;

    _msg_queue_push_back(proc_q,pMsg);

}

void 
systemcall_handler(sos_as *pcb, int num_args) {
    seL4_Word syscall_number;
    seL4_CPtr reply_cap;
    syscall_number = seL4_GetMR(0);

    /* Save the caller */
    reply_cap = cspace_save_reply_cap(cur_cspace);
    assert(reply_cap != CSPACE_NULL);
    /* Process system call */
    switch (syscall_number) {
        case SOS_SYSCALL_TIME_STAMP:
            systemcall_time_stamp(reply_cap);
            break;
        case SOS_SYSCALL_SLEEP:
            systemcall_register_sleep(pcb,reply_cap);
            break;
        case SOS_SYSCALL_OPEN:
            systemcall_open(pcb,reply_cap);
            break;
        case SOS_SYSCALL_CLOSE:
            systemcall_close(pcb,reply_cap);
            break;
        case SOS_SYSCALL_WRITE:
            systemcall_write(pcb,reply_cap);
            break;
        case SOS_SYSCALL_READ:
            systemcall_read(pcb,reply_cap);
            break;
        case SOS_SYSCALL_BRK:
            systemcall_brk(pcb,reply_cap);
            break;
        case SOS_SYSCALL_GETDIRENT:
            systemcall_getdirent(pcb,reply_cap);
            break;
        case SOS_SYSCALL_STAT:
            systemcall_stat(pcb,reply_cap);
            break;
        case SOS_SYSCALL_PROC_CREATE:
            systemcall_proc_create(pcb,reply_cap);
            break;
        case SOS_SYSCALL_PROC_DELETE:
            systemcall_proc_delete(pcb,reply_cap);
            break;
        case SOS_SYSCALL_PROC_STATUS:
            sos_pcb_process_status(pcb,reply_cap);
            break;
        case SOS_SYSCALL_PROC_WAIT:
            sos_pcb_process_wait(pcb,seL4_GetMR(1),reply_cap);
            break;
        case SOS_SYSCALL_PROC_PID:
            sos_reply_cap(reply_cap,pcb->pid);
            break;
        default:
            printf("Unknown syscall %d\n", syscall_number);
    }
}
static void
recycle_sub_thread(sos_as *pcb)
{
    struct msg* pMsg =(struct msg *)malloc(sizeof(struct msg));
    conditional_panic(!pMsg,"Failed to malloc msg to kill process after failed to handle vm fault");
    pMsg ->para_0 = SOS_SYSCALL_PROC_RECYCLE;
    pMsg ->para_1 = pcb;
    pMsg ->para_2 = pcb -> pid;
    _msg_queue_push_back(proc_q,pMsg);
}

void 
wrapper_system_loop()
{
    int err;
    sos_as *pcb = (sos_as *)seL4_GetMR(0);
    pcb->ipc_ut_addr = ut_alloc(seL4_EndpointBits);
    if(!pcb->ipc_ut_addr)
    {
        dprintf(0,"sub kernel_thread alloc ipc ep faile\n");
        recycle_pid(pcb->pid);
        if(pcb -> reply_cap)
            sos_reply_cap(pcb->reply_cap,-1);
        recycle_sub_thread(pcb);
        return;
    }
    err = cspace_ut_retype_addr(pcb->ipc_ut_addr,
            seL4_EndpointObject,
            seL4_EndpointBits,
            cur_cspace,
            &(pcb->ipc_ep));
    if(err)
    {
        ut_free(pcb->ipc_ut_addr,seL4_EndpointBits);
        recycle_pid(pcb->pid);
        if(pcb -> reply_cap)
            sos_reply_cap(pcb->reply_cap,-1);
        recycle_sub_thread(pcb);
        return;
    }

    err = create_application(pcb);

    if(err)
    {
        dprintf(0,"application create error ,do recycle\n");
        recycle_pid(pcb->pid);
        if(pcb -> reply_cap)
            sos_reply_cap(pcb->reply_cap,-1);
        cspace_delete_cap(cur_cspace,pcb -> ipc_ep);
        ut_free(pcb->ipc_ut_addr,seL4_EndpointBits);

        recycle_sub_thread(pcb);
        return;
    }

    pcb -> isready = 1;
    if(pcb -> reply_cap)
    {
        sos_reply_cap(pcb->reply_cap,pcb->pid);
        pcb -> reply_cap = 0;
    }
    systemcall_loop(pcb);
}

void 
systemcall_IRQ_loop()
{
    dprintf(0,"IRQ thread start\n");
    seL4_Word badge;
    seL4_CPtr ep = seL4_GetMR(0);
    while (1) {

        seL4_Wait(ep, &badge);
        if(badge & IRQ_EP_BADGE)
        {
            /* Interrupt */
            if (badge & IRQ_BADGE_NETWORK) {
                network_irq();
            }

            if(badge & IRQ_BADGE_CLOCK) {
                timer_interrupt();
            }
        }
        else
        {
            printf("IRQ server thread got an unknown message badge = %d\n",badge);
        }
    }
}

void
systemcall_sos_loop(seL4_CPtr ep)
{
    while (1) {
       struct msg *pMsg =  _msg_queue_pop_front(proc_q);
       switch(pMsg->para_0)
       {
           case SOS_SYSCALL_PROC_CREATE:
               sos_pcb_process_create((char *)pMsg->para_1,_sos_ipc_ep_cap,pMsg->reply_cap);
               break;
           case SOS_SYSCALL_PROC_DELETE:
               sos_pcb_process_delete((sos_as *)pMsg ->para_1,(pid_t)pMsg ->para_2,pMsg ->reply_cap);
               break;
           case SOS_SYSCALL_PROC_RECYCLE:
               sos_pcb_process_recycle((sos_as *)pMsg ->para_1,(pid_t)pMsg ->para_2);
               break;
       }
       free(pMsg);
    }
}

void 
systemcall_loop(sos_as *pcb) 
{
    seL4_CPtr ep;
    seL4_Word badge;
    seL4_Word label;
    seL4_MessageInfo_t message;
    while (1) {
        ep = pcb->ipc_ep;
        message = seL4_Wait(ep, &badge);
        label = seL4_MessageInfo_get_label(message);

        if(label == seL4_VMFault)
        {
            seL4_CPtr reply_cap;
            seL4_Uservaddr addr;

            dprintf(0,"Process : %d VM FAULT = %p,%p,%p\n",pcb->pid,seL4_GetMR(1),seL4_GetMR(2),seL4_GetMR(3));
            reply_cap = cspace_save_reply_cap(cur_cspace);
            addr = seL4_GetMR(1);
            if(pagetable_vm_fault(pcb,addr))
            {
                struct msg* pMsg =(struct msg *)malloc(sizeof(struct msg));
                conditional_panic(!pMsg,"Failed to malloc msg to kill process after failed to handle vm fault");
                pMsg ->para_0 = SOS_SYSCALL_PROC_DELETE;
                pMsg ->para_1 = pcb;
                pMsg ->para_2 = pcb->pid;
                pMsg ->reply_cap = reply_cap;
                _msg_queue_push_back(proc_q,pMsg);
            }
            else
                sos_reply_cap(reply_cap,0);
        }else if(label == seL4_NoFault) {
            /* System call */
            systemcall_handler(pcb, seL4_MessageInfo_get_length(message) - 1);

        }else{
            printf("Rootserver got an unknown message\n");
        }
    }
}


void notify_client(uint32_t id, void *data)
{
    sync_acquire(lock_arr[TIME_LOCK]) ;
    seL4_CPtr reply_cap = ((sos_as *) data)->reply_cap;
    sos_reply_cap(reply_cap,0);

    ((sos_as *) data)->time_id = 0;
    sync_release(lock_arr[TIME_LOCK]) ;
}

void systemcall_register_sleep(sos_as *pcb,seL4_CPtr reply_cap)
{
    int delay = seL4_GetMR(1);
    if(delay == 0)
    {
        sos_reply_cap(reply_cap,0);
        return;
    }
    else if(delay < 0)
    {
        sos_reply_cap(reply_cap,-1);
        return;
    }
    else
    {
        pcb->reply_cap = reply_cap;
        sync_acquire(lock_arr[TIME_LOCK]) ;
        pcb->time_id = register_timer((uint64_t)delay,&notify_client,(void*)pcb);
        sync_release(lock_arr[TIME_LOCK]) ;
    }
}

void systemcall_time_stamp(seL4_CPtr reply_cap)
{
    timestamp_t t = time_stamp();

    memcpy((void *) (seL4_GetIPCBuffer()->msg), &t, sizeof(timestamp_t));
    seL4_MessageInfo_t reply = seL4_MessageInfo_new(0, 0, 0, 3);
    seL4_Send(reply_cap, reply);
    cspace_free_slot(cur_cspace, reply_cap);
   
}

void systemcall_open(sos_as* pcb,seL4_CPtr reply_cap)
{
    seL4_File_Descripter local_id;
    seL4_Uservaddr path_uaddr;
    size_t length;
    char* path;
    fmode_t mode;

    mode = seL4_GetMR(1);
    length = seL4_GetMR(2);
    if(length == 0)
    {
        dprintf(0,"Failed to open due to path is null\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }
    path_uaddr = seL4_GetMR(3);
    if(path_uaddr == NULL)
    {
        dprintf(0,"Segment fault in open\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }
    path = ustr2sosstr(pcb,path_uaddr,length);
    if(!path)
    {
        dprintf(0,"Failed to malloc in system call open\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    for(local_id = 3;local_id < AS_FD_MAX;local_id ++)
    {
        if(pcb->fd_table[local_id] == -1)
            break;
    }

    mode = transform_file_permission(mode);
    if(mode == -1)
    {
        dprintf(0,"Failed to open file, open mode not correct\n");
        sos_reply_cap(reply_cap,-1);
        free(path);
        return;
    }

    if(local_id == AS_FD_MAX)
    {
        sos_reply_cap(reply_cap,-1);
        free(path);
    }
    else
        sos_vfs_open(pcb,local_id,path,mode,reply_cap);

}
void systemcall_close(sos_as *pcb,seL4_CPtr reply_cap)
{
    seL4_File_Descripter fd;
    fd = seL4_GetMR(1);

    sos_vfs_close(pcb,fd,reply_cap);
}

void systemcall_write(struct sos_as* pcb,seL4_CPtr reply_cap)
{
    size_t len;
    seL4_Uservaddr buf;
    seL4_File_Descripter fd;

    fd = seL4_GetMR(1);
    buf = seL4_GetMR(2);
    len = seL4_GetMR(3);

    if (len == 0)
    {
        sos_reply_cap(reply_cap,0);
        return;
    }
    if(fd < 0 || fd > AS_FD_MAX - 1)
    {
        dprintf(0,"File write error ,local fd = %d\n",fd );
        if(reply_cap) sos_reply_cap(reply_cap,-1);
        return;
    }
    if(pcb -> fd_table[fd] > -1)
        sos_vfs_write(pcb,pcb->fd_table[fd],buf,len,reply_cap);
    else
        if(reply_cap) sos_reply_cap(reply_cap,-1);
}

void systemcall_read(sos_as* pcb,seL4_CPtr reply_cap)
{
    size_t len;
    seL4_File_Descripter fd;
    seL4_Uservaddr buf;

    fd = seL4_GetMR(1);
    buf = seL4_GetMR(2);
    len = seL4_GetMR(3);
    if (len == 0)
    {
        sos_reply_cap(reply_cap,0);
        return;
    }
    if(fd < 0 || fd > AS_FD_MAX - 1)
    {
        dprintf(0,"File read error ,local fd = %d\n",fd );
        if(reply_cap) sos_reply_cap(reply_cap,-1);
        return;
    }

    if(pcb -> fd_table[fd] > -1)
        sos_vfs_read(pcb,pcb->fd_table[fd],buf,len,reply_cap);
    else
        if(reply_cap) sos_reply_cap(reply_cap,-1);

}

void systemcall_brk(sos_as* pcb,seL4_CPtr reply_cap)
{
    seL4_Uservaddr vaddr;
    vaddr = seL4_GetMR(1);
    if(vaddr >= PROCESS_HEAP_START 
            && vaddr < pcb->_stack->vbegin - PAGE_SIZE 
            && vaddr == PAGE_ALIGN(vaddr))
    {
        dprintf(0,"change heap end to = %p\n",vaddr);
        sos_pcb_resize_heap(pcb,vaddr);
    }

    sos_reply_cap(reply_cap,pcb->_heap->vend);
}

void systemcall_stat(struct sos_as* pcb,seL4_CPtr reply_cap)
{
    seL4_Buffer path;
    size_t length;
    seL4_Uservaddr path_uaddr;

    length = seL4_GetMR(1);
    if(length == 0)
    {
        dprintf(0,"Failed to open due to path is null\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    path_uaddr = seL4_GetMR(2);
    if(path_uaddr == NULL)
    {
        dprintf(0,"Segment fault in stat\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    path = ustr2sosstr(pcb,path_uaddr,length);
    if(!path)
    {
        dprintf(0,"Failed to malloc in system call stat\n");
        sos_reply_cap(reply_cap,-1);
        return;
    }

    sos_vfs_stat(path,reply_cap);
}

void systemcall_getdirent(sos_as* pcb,seL4_CPtr reply_cap)
{
    int pos;
    pos = seL4_GetMR(1);
    sos_vfs_getdirent(pos,reply_cap);
}

