#ifndef __FILE_CALLBACK__
#define __FILE_CALLBACK__
#include <sos.h>
#include <nfs/nfs.h>
typedef struct nfs_inode
{
    fhandle_t fh;
    fattr_t fattr;
}nfs_inode;

typedef struct 
{
    int isready;
    enum nfs_stat status;
    nfs_inode *inode;
}lookup_token,create_token;

typedef struct {
	int isready;
	enum nfs_stat status;
    enum rpc_stat err;
	fattr_t fattr;
	uint32_t count;
}write_token;

typedef struct {
	int isready;
	enum nfs_stat status;
    enum rpc_stat err;
	fattr_t fattr;
	uint32_t count;
    char* data;
}read_token;

typedef struct{
    int isready;
    enum nfs_stat status;
    char* name;
    nfscookie_t cookie;

    int history_num_files;
    int pos;
}readdir_token;

void _lookup_or_create_cb(uintptr_t token, enum nfs_stat status, fhandle_t *fh, fattr_t* fattr); 
void _write_cb(uintptr_t token, enum nfs_stat status, fattr_t* fattr, int count);
void _read_cb(uintptr_t token, enum nfs_stat status,fattr_t *fattr, int count, void* data);
void _readdir_cb(uintptr_t token, enum nfs_stat status,int num_files, char* file_names[],nfscookie_t nfscookie);


#endif
