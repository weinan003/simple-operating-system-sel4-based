#include <autoconf.h>
#include <ut_manager/ut.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <cspace/cspace.h>

#include "frametable.h"
#include "task.h"
#include "vmem_layout.h"


#define verbose 5
#include <sys/debug.h>
#include <sys/panic.h>

#define TASK_MAX_NUM 10

static uint32_t task_vaddr = PROCESS_SCRATCH;
extern union u_frame* frametable;

static 
inline int 
task_init_ipc(task_node *node)
{
    int err;
    node -> ipc_addr = ut_alloc(seL4_PageBits);
    if(!(node -> ipc_addr))
    {
        dprintf(0,"ut alloc failed in task init ipc\n");
        return 1;
    }
    err = cspace_ut_retype_addr(node -> ipc_addr,
            seL4_ARM_SmallPageObject,seL4_PageBits,
            cur_cspace,
            &(node -> ipc_cap));
    if(err)
    {
        dprintf(0,"retype failed in task init ipc\n");
        ut_free(node -> ipc_addr,seL4_PageBits);
        return err;
    }

    node -> ipc_vaddr = task_vaddr;

    err = map_page(node-> ipc_cap,seL4_CapInitThreadPD,
            node -> ipc_vaddr,
            seL4_AllRights,seL4_ARM_PageCacheable);
    if(err)
    {
        dprintf(0,"map failed in task init ipc\n");
        cspace_delete_cap(cur_cspace,node->ipc_cap);
        ut_free(node -> ipc_addr,seL4_PageBits);
        return err;
    }

    task_vaddr += PAGE_SIZE;
    return 0;
}

static 
inline int
task_init_tcb(task_node *node,seL4_CPtr fault_ep)
{
    int err;
    node->tcb_addr = ut_alloc(seL4_TCBBits);
    if(!(node -> tcb_addr))
    {
        dprintf(0,"ut alloc failed in task init tcb\n");
        return 1;
    }
    err = cspace_ut_retype_addr(node -> tcb_addr,seL4_TCBObject,seL4_TCBBits,
            cur_cspace,
            &(node -> tcb_cap));
    if( err)
    {
        dprintf(0,"retype failed in task init tcb\n");
        ut_free(node -> tcb_addr,seL4_TCBBits);
        return err;
    }

    err = seL4_TCB_Configure(node -> tcb_cap, fault_ep, 255,
            seL4_CapInitThreadCNode, seL4_NilData,
            seL4_CapInitThreadVSpace, seL4_NilData, 
            node -> ipc_vaddr,
            node -> ipc_cap);
    if(err)
    {
        dprintf(0,"configure tcb failed in task init tcb\n");
        cspace_delete_cap(cur_cspace,node->tcb_cap);
        ut_free(node -> tcb_addr,seL4_TCBBits);
        return err;
    }
    return 0;

}

static 
inline int 
task_init_stack(task_node *node)
{
    int err;
    int j = 0;
    for(j = 0;j < 3;j ++)
    {
        node ->stack_addr[j] = ut_alloc(seL4_PageBits);
        if(!(node -> stack_addr[j]))
        {
            dprintf(0,"failed do ut alloc in stack init stack\n");
            return 1;
        }
        err = cspace_ut_retype_addr(node -> stack_addr[j],
                seL4_ARM_SmallPageObject,seL4_PageBits,
                cur_cspace,
                &(node ->stack_cap[j]));
        if( err )
        {
            dprintf(0,"failed do retype in stack init stack\n");
            ut_free(node -> stack_addr[j],seL4_PageBits);
            return err;
        }

        seL4_Word stack_buffer_vaddr;
        stack_buffer_vaddr = task_vaddr;

        err = map_page(node->stack_cap[j],seL4_CapInitThreadPD,
                stack_buffer_vaddr,
                seL4_AllRights,seL4_ARM_PageCacheable);
        if( err )
        {
            dprintf(0,"failed do map in stack init stack\n");
            cspace_delete_cap(cur_cspace,node -> stack_cap[j]);
            ut_free(node -> stack_addr[j],seL4_PageBits);
            return err;
        }


        task_vaddr += PAGE_SIZE;
    }
    return 0;
}

static 
inline void 
task_start(task_node *node,task_fun tfunction,void *para)
{
    int err;
    uintptr_t stack_top = task_vaddr;
    size_t reg_size = sizeof(seL4_UserContext) / sizeof(seL4_Word);
    seL4_UserContext context = {
        .pc =  (seL4_Word) tfunction,
        .sp = (seL4_Word) stack_top
    };
    seL4_IPCBuffer *ipc = (seL4_IPCBuffer *)node -> ipc_vaddr;
    ipc->msg[0] = para;
    err = seL4_TCB_WriteRegisters(node -> tcb_cap,1,0,reg_size,&context);
    conditional_panic(err,"Failed to write tcb register");
}

task_node*
sos_task_create(task_fun tfunction,seL4_CPtr fault_ep,void *para,int badge,int id)
{
    int err ;
    task_node *node;
    /* check task pool if have space */
    node = (task_node *)malloc(sizeof(task_node));
    if(!node) return NULL;

    seL4_CPtr user_ep_cap ;
    /* task is a sos subthread ,share cspace and vspace with sos. the virtual address is after PROCESS_SCRATCH */
    user_ep_cap = cspace_mint_cap(cur_cspace,
            cur_cspace,
            fault_ep,
            seL4_AllRights,
            seL4_CapData_Badge_new(badge));

    task_vaddr = PROCESS_SCRATCH + id * 4 * PAGE_SIZE;
    dprintf(0,"task id = %d,start page = %p\n",id,task_vaddr);

    err = task_init_ipc(node);
    if(err)
    {
        free(node);
        return NULL;
    }
    err = task_init_tcb(node ,user_ep_cap);
    if(err)
    {
        cspace_delete_cap(cur_cspace,node->ipc_cap);
        ut_free(node -> ipc_addr,seL4_PageBits);
        free(node);
        return NULL;
    }
    task_init_stack(node);
    task_start(node ,tfunction,para);

    return node;
}

void 
sos_task_destory(task_node* node)
{
    for(int j = 0;j < 3;j ++)
    {
        seL4_ARM_Page_Unmap(node ->stack_cap[j]);
        cspace_delete_cap(cur_cspace,node ->stack_cap[j]);
        ut_free(node ->stack_addr[j],seL4_PageBits);
    }

    seL4_ARM_Page_Unmap(node ->ipc_cap);
    cspace_delete_cap(cur_cspace,node ->ipc_cap);
    ut_free(node ->ipc_addr,seL4_PageBits);

    cspace_delete_cap(cur_cspace,node ->tcb_cap);
    ut_free(node ->tcb_addr,seL4_TCBBits);

    free(node);
}

void* 
sync_new_ep(seL4_CPtr* ep, int badge)
{
    int err;
    msg_ep* m_ep ;
    m_ep = (msg_ep*) malloc(sizeof(msg_ep));
    if(!m_ep) 
    {
        dprintf(0,"Failed to alloc endpoint struct in sync_new_ep\n");
        return NULL;
    }

    m_ep->addr = ut_alloc(seL4_EndpointBits);
    if(!(m_ep->addr))
    {
        dprintf(0,"Failed to alloc endpoint in sync_new_ep\n");
        free(m_ep);
        return NULL;
    }

    err = cspace_ut_retype_addr(m_ep->addr,seL4_AsyncEndpointObject,seL4_EndpointBits,
            cur_cspace,
            &(m_ep->cap));
    if (err)
    {
        dprintf(0,"Failed to retype endpoint in sync_new_ep\n");
        ut_free(m_ep->addr,seL4_EndpointBits);
        free(m_ep);
        return NULL;
    }

    *ep = cspace_mint_cap(cur_cspace,cur_cspace,m_ep->cap,seL4_AllRights,seL4_CapData_Badge_new(badge));

    return m_ep;
}

void 
sync_free_ep(void* ep)
{
    msg_ep *m_ep ;
    m_ep = (msg_ep *)ep;
    cspace_recycle_cap(cur_cspace,m_ep->cap); 
    cspace_delete_cap(cur_cspace,m_ep->cap);
    ut_free(m_ep->addr,seL4_EndpointBits);
    free(ep);
}

msg_queue* 
_msg_queue_init(void)
{
    msg_queue *queue = (msg_queue *)malloc(sizeof(msg_queue));
    if(!queue) return NULL;

    queue->head_ = (struct msg *)malloc(sizeof(struct msg));
    if(!(queue->head_))
    {
        free(queue);
        return NULL;
    }
    queue->tail_ = queue->head_;

    queue->_sem = sync_create_sem(0); 
    if(!(queue->_sem))
    {
        free(queue->head_);
        free(queue);
        return NULL;
    }

    queue->_mux = sync_create_mutex(); 
    if(!(queue->_mux))
    {
        sync_destroy_sem(queue->_sem);
        free(queue->head_);
        free(queue);
        return NULL;
    }
    return queue;
}

struct msg* 
_msg_queue_pop_front(msg_queue * q)
{
    struct msg *p_Msg;
    sync_wait(q -> _sem);
    sync_acquire(q -> _mux);
    p_Msg = q -> head_ -> next;
    if(p_Msg == q -> tail_)
        q -> tail_ = q -> head_;
    else
        q -> head_ -> next = p_Msg -> next;
    sync_release(q -> _mux);
    return p_Msg;
}

void 
_msg_queue_push_back(msg_queue * q,struct msg * m)
{
    sync_acquire(q -> _mux);
    q -> tail_ -> next = m;
    q -> tail_ = q -> tail_ -> next;
    sync_release(q -> _mux);
    sync_signal(q -> _sem);
}
