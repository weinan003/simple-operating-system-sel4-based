#ifndef __FRAMETABLE__
#define __FRAMETABLE__
#include <sel4/sel4.h>
#include <stdint.h>
#include <sync/mutex.h>
#include "env.h"
#include "pcb.h"
#define BITREF (1<<31)
#define BITPIN (1<<30)
#define MAX_PHY_PAGE_SIZE (80000)
//#define MAX_PHY_PAGE_SIZE (100)
//#define MAX_PHY_PAGE_SIZE (900)

struct sos_as;
union u_frame
{
    struct{
        union u_frame* nextfree_;
    }head;
    struct {
        uint32_t 		ref;
        seL4_CPtr 		cap;
        seL4_Word 		addr;
        struct sos_as*	pcb;
        seL4_Uservaddr	va;
    }n;
};

int FRAMETABLE_SIZE;


void frame_init();
uint32_t frame_alloc(struct sos_as *pcb, seL4_Uservaddr vaddr);
seL4_Sosvaddr frame_seek_sos_addr(uint32_t phy_id);
uint32_t frame_seek_sos_cap(uint32_t phy_id);
uint32_t frame_seek_phy_id(uint32_t kvaddr);
void frame_free(sos_as* pcb,uint32_t id);

/* 检查frame的ref bit
 * 返回TRUE 表示ref设为1, FALSE表示ref没有设置*/
BOOL frame_check_ref(int index);

/* 修改frame的ref值 */
void set_frame_ref(int index);
void unset_frame_ref(int index);


BOOL frame_check_pin(int index);
void set_frame_pin(int index);
void unset_frame_pin(int index);

BOOL frame_check_alloc(int index);
struct sos_as* frame_seek_pcb(int index);
seL4_Uservaddr frame_seek_Userpage(int index);

void frame_set_pcb(int index,sos_as *pcb,seL4_Uservaddr vaddr);
#endif
