#ifndef __SWAP_TABLE__
#define __SWAP_TABLE__
#include <sel4/sel4.h>
#include <stdint.h>
#include <nfs/nfs.h>
#include "env.h"
#include "pagetable.h"
/* swap factor是需要交换出去的page数目 */
#define SWAP_FACTOR 20 
struct sos_as;
extern fhandle_t NFS_SWAP_FILE;

typedef seL4_CPtr PCB_Ptr;
union u_swap
{
    struct{
        union u_swap* nextfree_;
    }head;
    struct {
        int occupy_flag;
    }n;
};

union u_swap *swap_table, *swap_freehead;

void swap_init();
int swap_file_init();
int swap_pop_free_node(struct sos_as* pcb);
void swap_recycle_node(struct sos_as* pcb,int index);
uint32_t swap_get_file_offset(int index);
seL4_Sosvaddr _convert_frameIndex_to_sosVaddr(int index);

/* swapin */
int swapin(pageinfo pi, int swaptable_index,struct sos_as *pcb, seL4_Uservaddr va);
uint32_t get_swaptable_index(pageinfo pi);
uint32_t check_page_status(pageinfo pi);

uint32_t swapout(struct sos_as *pcb, seL4_Uservaddr va);

int _choose_victim();
#endif
